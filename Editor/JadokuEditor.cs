﻿#if UNITY_2018_0_OR_NEWER
using Jadoku.GameObjectBrush;
#endif
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class JadokuEditor {
#region asset bundling
    [MenuItem("Jadoku/Asset Bundles/Buildng/Windows")]
    static void build_asset_bundles_windows() {
        if(!Directory.Exists("AssetBundles")) {
            Directory.CreateDirectory("AssetBundles");
        }
        if(!Directory.Exists("AssetBundles/win")) {
            Directory.CreateDirectory("AssetBundles/win");
        }
        BuildPipeline.BuildAssetBundles("AssetBundles/win", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows64);
    }

    [MenuItem("Jadoku/Asset Bundles/Buildng/Linux")]
    static void build_asset_bundles_linux() {
        if(!Directory.Exists("AssetBundles")) {
            Directory.CreateDirectory("AssetBundles");
        }
        if(!Directory.Exists("AssetBundles/linux")) {
            Directory.CreateDirectory("AssetBundles/linux");
        }
        BuildPipeline.BuildAssetBundles("AssetBundles/linux", BuildAssetBundleOptions.None, BuildTarget.StandaloneLinux64);
    }

    [MenuItem("Jadoku/Asset Bundles/Buildng/Android")]
    static void build_asset_bundles_android() {
        if(!Directory.Exists("AssetBundles")) {
            Directory.CreateDirectory("AssetBundles");
        }
        if(!Directory.Exists("AssetBundles/android")) {
            Directory.CreateDirectory("AssetBundles/android");
        }
        BuildPipeline.BuildAssetBundles("AssetBundles/android", BuildAssetBundleOptions.None, BuildTarget.Android);
    }

    [MenuItem("Jadoku/Asset Bundles/Buildng/Ios")]
    static void build_asset_bundles_ios() {
        if(!Directory.Exists("AssetBundles")) {
            Directory.CreateDirectory("AssetBundles");
        }
        if(!Directory.Exists("AssetBundles/ios")) {
            Directory.CreateDirectory("AssetBundles/ios");
        }
        BuildPipeline.BuildAssetBundles("AssetBundles/ios", BuildAssetBundleOptions.None, BuildTarget.iOS);
    }

    [MenuItem("Jadoku/Asset Bundles/Buildng/WebGL")]
    static void build_asset_bundles_webgl() {
        if(!Directory.Exists("AssetBundles")) {
            Directory.CreateDirectory("AssetBundles");
        }
        if(!Directory.Exists("AssetBundles/webgl")) {
            Directory.CreateDirectory("AssetBundles/webgl");
        }
        BuildPipeline.BuildAssetBundles("AssetBundles/webgl", BuildAssetBundleOptions.None, BuildTarget.WebGL);
    }

    [MenuItem("Jadoku/Asset Bundles/Clear local cache")]
    static void ClearDownloadedCache() {
        Caching.ClearCache();
        UnityEngine.Debug.Log("Cache cleared");
    }
#endregion

    [MenuItem("Jadoku/Misc/Clear player prefs")]
    static void misc_clear_player_prefs() {
        PlayerPrefs.DeleteAll();
    }
}
