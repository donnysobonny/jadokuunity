﻿using Jadoku.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Jadoku {
    [CustomEditor(typeof(FaderEscaper))]
    public class FaderEscaperEditor : Editor {

        SerializedProperty edit;

        ReorderableList faders;

        SerializedProperty onDefault;

        void OnEnable() {
            this.edit = this.serializedObject.FindProperty("edit");
            this.onDefault = this.serializedObject.FindProperty("onDefault");

            this.faders = new ReorderableList(this.serializedObject,
                    this.serializedObject.FindProperty("faders"),
                    true, false, true, true);
            this.faders.elementHeightCallback = delegate (int index) {
                return (EditorGUIUtility.singleLineHeight * 1) + 2;
            };
            this.faders.drawElementCallback = delegate (Rect rect, int index, bool isActive, bool isFocused) {
                var element = this.faders.serializedProperty.GetArrayElementAtIndex(index);
                //spacing
                rect.y += 2;

                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
                    element
                );
            };
            this.faders.drawHeaderCallback = delegate (Rect rect) {
                EditorGUI.LabelField(rect, new GUIContent("Faders"));
            };    
        }

        public override void OnInspectorGUI() {
            this.serializedObject.Update();

            EditorGUILayout.PropertyField(this.edit);

            if(this.edit.boolValue) {
                EditorGUILayout.HelpBox("List the faders in order below. When esc/back is pressed, the first fader in the list is faded in will be faded out. If all faders are faded out, the On Default event below will be called", MessageType.Info);
                this.faders.DoLayoutList();
                EditorGUILayout.HelpBox("If no faders in the above list are faded in, this event is called. It is usually best just to exit the game within this event", MessageType.Info);
                EditorGUILayout.PropertyField(this.onDefault);
            }

            this.serializedObject.ApplyModifiedProperties();
        }
    }
}

