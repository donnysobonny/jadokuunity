﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Jadoku.AssetBundles {
    [CustomEditor(typeof(AssetBundleManager))]
    public class AssetBundleManagerEditor : Editor {
        SerializedProperty assetBundleLocation;
        SerializedProperty useJsonSchema;
        SerializedProperty jsonSchemaUrl;

        ReorderableList versionedAssetBundles;

        void OnEnable() {
            this.assetBundleLocation = this.serializedObject.FindProperty("assetBundleLocation");
            this.useJsonSchema = this.serializedObject.FindProperty("useJsonSchema");
            this.jsonSchemaUrl = this.serializedObject.FindProperty("jsonSchemaUrl");

            this.versionedAssetBundles = new ReorderableList(this.serializedObject,
                    this.serializedObject.FindProperty("versionedAssetBundles"),
                    true, false, true, true);
            this.versionedAssetBundles.elementHeightCallback = delegate (int index) {
                return (EditorGUIUtility.singleLineHeight * 2) + 2;
            };
            this.versionedAssetBundles.drawElementCallback = delegate (Rect rect, int index, bool isActive, bool isFocused) {
                var element = this.versionedAssetBundles.serializedProperty.GetArrayElementAtIndex(index);
                //spacing
                rect.y += 2;
                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("name")
                );

                rect.y += EditorGUIUtility.singleLineHeight;

                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("version")
                );
            };
            this.versionedAssetBundles.drawHeaderCallback = delegate (Rect rect) {
                EditorGUI.LabelField(rect, new GUIContent("Versioned Asset Bundles"));
            };
        }

        public override void OnInspectorGUI() {
            this.serializedObject.Update();

            EditorGUILayout.PropertyField(this.assetBundleLocation);
            EditorGUILayout.PropertyField(this.useJsonSchema);

            if(this.useJsonSchema.boolValue) {
                EditorGUILayout.PropertyField(this.jsonSchemaUrl);
            } else {
                this.versionedAssetBundles.DoLayoutList();
            }

            this.serializedObject.ApplyModifiedProperties();
        }
    }
}
