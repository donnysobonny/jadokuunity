mergeInto(LibraryManager.library, {
    WebGLCookies_Set: function(cookieName,cookieValue,days) {
        var name = Pointer_stringify(cookieName);
        var value = Pointer_stringify(cookieValue);
        Cookies.set(name, value, { expires: days });
    },
    WebGLCookies_Get: function(cookieName) {
        var name = Pointer_stringify(cookieName);
        var value = Cookies.get(name);

        var ret = "";
        if(value) {
            ret = value;
        }

        var buffer = _malloc(lengthBytesUTF8(ret) + 1);
        writeStringToMemory(ret, buffer);
        return buffer;
    }
});