﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jadoku.Utils {
    public class Move2D : MonoBehaviour {

        public float moveSpeed = 10f;

        // Update is called once per frame
        void Update() {
            this.transform.position += new Vector3(
                Input.GetAxis("Horizontal") * Time.deltaTime * this.moveSpeed,
                Input.GetAxis("Vertical") * Time.deltaTime * this.moveSpeed,
                0f    
            );
        }
    }
}