﻿using System.Text;
using UnityEngine;

namespace Jadoku.Utils {
    public class StatisticsGUI : MonoBehaviour {

        public bool displayFps;
        public bool displayGraphicsTeir;
        public bool displayQualityLevel;

        private StringBuilder sb = new StringBuilder();
        private float fps;
        private void OnGUI() {
            sb.Length = 0;
            if(this.displayFps) {
                sb.AppendFormat("FPS: {0:n2}\n", this.fps);
            }
            if(this.displayGraphicsTeir) {
                sb.AppendFormat("Tier: {0}\n", Graphics.activeTier);
            }
            if(this.displayQualityLevel) {
                sb.AppendFormat("Quality: {0}\n", QualitySettings.GetQualityLevel());
            }
            GUI.Label(new Rect(0, 0, 500, 500), sb.ToString());
        }

        private void Update() {
            if(this.displayFps) {
                this.fps = 1f / Time.smoothDeltaTime;
            }
        }
    }
}