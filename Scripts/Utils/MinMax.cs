﻿using System;

namespace Jadoku.Utils {
    [Serializable]
    public class MinMax {
        public float min;
        public float max;

        public MinMax() { }
        public MinMax(float min, float max) {
            this.min = min;
            this.max = max;
        }
        public MinMax(MinMax clone) {
            this.min = clone.min;
            this.max = clone.max;
        }

        /// <summary>
        /// Copy the values from another MinMax instance
        /// </summary>
        /// <param name="copy"></param>
        public void CopyFrom(MinMax copy) {
            this.min = copy.min;
            this.max = copy.max;
        }

        /// <summary>
        /// Clone this instance
        /// </summary>
        /// <returns></returns>
        public MinMax Clone() {
            return new MinMax(this);
        }

        /// <summary>
        /// Get the range between max and min
        /// </summary>
        /// <returns></returns>
        public float GetRange() {
            return this.max - this.min;
        }

        /// <summary>
        /// Get a value between minimum and maximum using a normalized offset (a value between 0 and 1)
        /// 
        /// At 0, min will be returned while at 1, max will be returned.
        /// </summary>
        /// <param name="normalizedOffset">A value between 0 and 1</param>
        /// <returns></returns>
        public float GetValue(float normalizedOffset) {
            if(normalizedOffset < 0f) {
                normalizedOffset = 0f;
            } else if(normalizedOffset > 1f) {
                normalizedOffset = 1f;
            }
            return this.min + ((this.max - this.min) * normalizedOffset);
        }

        /// <summary>
        /// Get the normalized value of value, between min and max
        /// 
        /// If value = min, then 0 is returned. If value = max then 1 is returned
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public float GetValueNormalized(float value) {
            float cvalue = this.Clamp(value);
            return (cvalue - this.min) / (this.max - this.min);
        }

        /// <summary>
        /// Get a random value in range
        /// </summary>
        /// <returns></returns>
        public float Random() {
            return this.GetValue(UnityEngine.Random.Range(0f, 1f));
        }

        /// <summary>
        /// Get a random value between a minimum and maximum normalized value
        /// </summary>
        /// <param name="minNormalized"></param>
        /// <param name="maxNormalized"></param>
        /// <returns></returns>
        public float RandomRange(float minNormalized, float maxNormalized) {
            return UnityEngine.Random.Range(this.GetValue(minNormalized), this.GetValue(maxNormalized));
        }

        /// <summary>
        /// Detect whether a value is between min and max, inclusive of value (>= and <=)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool InRangeInclusive(float value) {
            return value >= this.min && value <= this.max;
        }

        /// <summary>
        /// Detect whether a value is between min and max, exclusive of value (> and <)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool InRangeExclusive(float value) {
            return value > this.min && value < this.max;
        }

        /// <summary>
        /// Clamp a value between min and max
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public float Clamp(float value) {
            if(value < this.min) {
                value = this.min;
            } else if(value > this.max) {
                value = this.max;
            }
            return value;
        }

        public void Multiply(float value) {
            this.min *= value;
            this.max *= value;
        }
        public void Add(float value) {
            this.min += value;
            this.max += value;
        }
    }
}
