﻿namespace Jadoku.Utils {
    public class ULongPool {
        ulong value;

        public ULongPool() { }
        public ULongPool(ulong start) {
            this.value = start;
        }

        public ulong GetNext() {
            if(this.value >= ulong.MaxValue) {
                this.value = 0;
            }
            this.value++;
            return this.value;
        }
    }
}
