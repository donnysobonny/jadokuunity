﻿using UnityEngine;

namespace Jadoku.Utils {
    public class LookAt : MonoBehaviour {
        public Transform target;
        public LookAtWorldDirection worldUpDirection;
        public Vector3 offsetRotation = Vector3.zero;
        
        private void Update() {
            if(this.target != null) {
                switch(this.worldUpDirection) {
                    case LookAtWorldDirection.up:
                        this.transform.LookAt(this.target, Vector3.up);
                        break;
                    case LookAtWorldDirection.down:
                        this.transform.LookAt(this.target, Vector3.down);
                        break;
                    case LookAtWorldDirection.left:
                        this.transform.LookAt(this.target, Vector3.left);
                        break;
                    case LookAtWorldDirection.right:
                        this.transform.LookAt(this.target, Vector3.right);
                        break;
                    case LookAtWorldDirection.forward:
                        this.transform.LookAt(this.target, Vector3.forward);
                        break;
                    case LookAtWorldDirection.back:
                        this.transform.LookAt(this.target, Vector3.back);
                        break;
                }
            }
            this.transform.eulerAngles += this.offsetRotation;
        }
    }

    public enum LookAtWorldDirection {
        up,
        down,
        left,
        right,
        forward,
        back
    }
}
