﻿using System;
using UnityEngine;

namespace Jadoku.Utils {
    public class GameObjectPool {
        GameObject[] pool;

        GameObject prefab;

        /// <summary>
        /// Construct a new object pool, specifying the prefab used to construct the game object and the initial size of the pool.
        /// 
        /// Note that the gameobject must contain the T component.
        /// Also note that the initial pool size should be the maximum size that you need. The pool will be resized if it's not big enough
        /// but at the expense of performance. So avoid this if possible
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="initialPoolSize"></param>
        public GameObjectPool(GameObject prefab, int initialPoolSize = 0) {
            this.prefab = prefab;
            this.pool = new GameObject[initialPoolSize];
            for(int i = 0; i < this.pool.Length; i++) {
                this.pool[i] = UnityEngine.Object.Instantiate(this.prefab);
                this.pool[i].SetActive(false);
            }
        }

        /// <summary>
        /// Get an object from the pool. Note that this immediately enables the game object in the scene.
        /// </summary>
        /// <returns></returns>
        public GameObject GetObject() {
            for(int i = 0; i < this.pool.Length; i++) {
                if(!this.pool[i].activeInHierarchy) {
                    this.pool[i].SetActive(true);
                    return this.pool[i];
                }
            }

            //no object found, increase the pool by one
            Debug.LogWarningFormat("The game object pool for the type {0} had to be resized. You should initiate this object pool with a larger size.", this.prefab.name);
            Array.Resize<GameObject>(ref this.pool, this.pool.Length + 1);
            this.pool[this.pool.Length - 1] = UnityEngine.Object.Instantiate(this.prefab);
            this.pool[this.pool.Length - 1].SetActive(true);
            return this.pool[this.pool.Length - 1];
        }
    }
}
