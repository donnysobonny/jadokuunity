﻿namespace Jadoku.Utils {
    public class IntPool {
        int value;

        public IntPool() { }
        public IntPool(int start) {
            this.value = start;
        }

        public int GetNext() {
            if(this.value >= int.MaxValue) {
                this.value = 0;
            }
            this.value++;
            return this.value;
        }
    }
}
