﻿using UnityEngine;

namespace Jadoku.Utils {
    public class Shake2D : MonoBehaviour {
        public float directionChangeInterval = 0.1f;
        public float shakeDistance = 0.1f;
        public float shakeLifetime = 1f;

        private float currentDistance = 0f;
        private float currentLifetime = 0f;
        private float totalLifetime = 0f;
        public void Shake(float amount = 1f, float lifeTime = 1f) {
            this.currentDistance = this.shakeDistance * amount;
            this.currentLifetime = this.totalLifetime = this.shakeLifetime * lifeTime;
        }

        private float nextChange = 0f;
        private Vector2 dir = Vector2.zero;
        private void Update() {
            if(this.currentLifetime > 0f) {
                if(Time.time > this.nextChange) {
                    this.nextChange = Time.time + this.directionChangeInterval;
                    this.dir = new Vector2(
                        Random.Range(-1f, 1f),
                        Random.Range(-1f, 1f)
                    ) * this.currentDistance * (this.currentLifetime / this.totalLifetime);
                }
                this.transform.localPosition = Vector2.Lerp(this.transform.localPosition, this.dir, Time.deltaTime);
                this.currentLifetime -= Time.deltaTime;
            } else {
                this.transform.localPosition = Vector2.Lerp(this.transform.localPosition, Vector2.zero, Time.deltaTime);
            }
        }
    }
}
