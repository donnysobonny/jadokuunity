﻿using UnityEngine;

namespace Jadoku.Utils {
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteFitToScreen : MonoBehaviour {

        public SpriteRenderer spriteRenderer;

        [Header("The scale mode used to scale the sprite" +
            "\n" +
            "\n" +
            "height: only scale the height of the sprite to fit the height of the screen\n" +
            "width: only scale the width of the sprite to fit the width of the screen\n" +
            "smallest: scale with the smallest side of the screen. This is useful for full-screen sprites where you need to keep the aspect ratio\n" +
            "both: scale both the screen and the height to fit the screen. This is useful for full screen sprites where you don't need to worry about the aspect ratio")]
        public SpriteFitToScreenScaleMode scaleMode = SpriteFitToScreenScaleMode.smallest;
        [Header("Select how to scale the width and height of the sprite" +
            "\n" +
            "\n" +
            "transform: scale the transform of the sprite to fit the screen\n" +
            "tiling: scale the tiling of the sprite to fit the screen")]
        public SpriteFitToScreenScaleWith scaleWidthWith = SpriteFitToScreenScaleWith.transform;
        public SpriteFitToScreenScaleWith scaleHeightWith = SpriteFitToScreenScaleWith.transform;

        public bool runOnStart = true;
        public bool runOnUpdate = false;

        private void OnDrawGizmos() {
            if(this.spriteRenderer == null) {
                this.spriteRenderer = this.GetComponent<SpriteRenderer>();
            }
        }

        // Use this for initialization
        void Start() {
            if(this.runOnStart) {
                this.Process();
            }
        }

        // Update is called once per frame
        void Update() {
            if(this.runOnUpdate) {
                this.Process();
            }
        }

        public void Process() {
            switch(this.scaleMode) {
                case SpriteFitToScreenScaleMode.height:
                    this.ScaleHeight();
                    break;
                case SpriteFitToScreenScaleMode.width:
                    this.ScaleWidth();
                    break;
                case SpriteFitToScreenScaleMode.smallest:
                    if(this.GetScreenWidthInWorld() < this.GetScreenHeightInWorld()) {
                        this.ScaleWidth(true);
                    } else if(this.GetScreenHeightInWorld() < this.GetScreenWidthInWorld()) {
                        this.ScaleHeight(true);
                    }
                    break;
                case SpriteFitToScreenScaleMode.both:
                    this.ScaleHeight();
                    this.ScaleWidth();
                    break;
            }
        }

        private void ScaleHeight(bool keepAspect = false) {
            float diff = this.GetScreenHeightInWorld() / this.GetSpriteHeightInWorld();
            
            switch(this.scaleHeightWith) {
                case SpriteFitToScreenScaleWith.transform:
                    if (keepAspect) {
                        this.transform.localScale = new Vector3(
                            diff,
                            diff,
                            this.transform.localScale.z
                        );
                    } else {
                        this.transform.localScale = new Vector3(
                            this.transform.localScale.x,
                            diff,
                            this.transform.localScale.z
                        );
                    }
                    break;
                case SpriteFitToScreenScaleWith.tiling:
                    if (keepAspect) {
                        this.spriteRenderer.size += new Vector2(
                            diff,
                            diff
                        );
                    } else {
                        this.spriteRenderer.size += new Vector2(
                            this.spriteRenderer.size.x,
                            diff
                        );
                    }
                    break;
            }
        }
        private void ScaleWidth(bool keepAspect = false) {
            float diff = this.GetScreenWidthInWorld() / this.GetSpriteWidthInWorld();
            
            switch(this.scaleWidthWith) {
                case SpriteFitToScreenScaleWith.transform:
                    if (keepAspect) {
                        this.transform.localScale = new Vector3(
                            diff,    
                            diff,
                            this.transform.localScale.z
                        );
                    } else {
                        this.transform.localScale = new Vector3(
                            diff,    
                            this.transform.localScale.y,
                            this.transform.localScale.z
                        );
                    }
                    break;
                case SpriteFitToScreenScaleWith.tiling:
                    if (keepAspect) {
                        this.spriteRenderer.size = new Vector2(
                            diff,    
                            diff
                        );
                    } else {
                        this.spriteRenderer.size = new Vector2(
                            diff,    
                            this.spriteRenderer.size.y
                        );
                    }
                    break;
            }
        }

        public float GetSpriteWidthInWorld() {
            return this.spriteRenderer.bounds.size.x;
        }

        public float GetSpriteHeightInWorld() {
            return this.spriteRenderer.bounds.size.y;
        }

        public float GetScreenHeightInWorld() {
            return Camera.main.orthographicSize * 2f;
        }
        public float GetScreenWidthInWorld() {
            return this.GetScreenHeightInWorld() / Screen.height * Screen.width;
        }
    }

    public enum SpriteFitToScreenScaleMode {
        height,
        width,
        smallest,
        both
    }
    public enum SpriteFitToScreenScaleWith {
        transform,
        tiling
    }
}