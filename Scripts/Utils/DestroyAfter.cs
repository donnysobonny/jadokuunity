﻿using UnityEngine;

namespace Jadoku.Utils {
    public class DestroyAfter : MonoBehaviour {

        public float lifeTime = 5f;

        // Use this for initialization
        void Start() {
            Destroy(this.gameObject, this.lifeTime);
        }
    }
}
