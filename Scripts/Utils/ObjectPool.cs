﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Jadoku.Utils {
    public class ObjectPool<T> where T : MonoBehaviour {
        T[] pool;
        T prefab;

        /// <summary>
        /// Construct a new object pool, specifying the prefab used to construct the game object and the initial size of the pool.
        /// 
        /// Note that the gameobject must contain the T component.
        /// Also note that the initial pool size should be the maximum size that you need. The pool will be resized if it's not big enough
        /// but at the expense of performance. So avoid this if possible
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="initialPoolSize"></param>
        public ObjectPool(T prefab, int initialPoolSize = 0, Transform parent = null) {
            this.prefab = prefab;
            this.pool = new T[initialPoolSize];
            for(int i = 0; i < this.pool.Length; i++) {
                this.pool[i] = UnityEngine.Object.Instantiate(this.prefab, parent);
                this.pool[i].gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Get an object from the pool disabled.
        /// 
        /// WARNING! You must immediately enable the object as soon as you get it from the pool, otherwise the next GetObject call will return the same object!
        /// </summary>
        /// <returns></returns>
        public T GetObjectDisabled() {
            for(int i = 0; i < this.pool.Length; i++) {
                if(!this.pool[i].gameObject.activeInHierarchy) {
                    return this.pool[i];
                }
            }

            //no object found, increase the pool by one
            Debug.LogWarningFormat("The object pool for the type {0} had to be resized. You should initiate this object pool with a larger size.", typeof(T).Name);
            Array.Resize<T>(ref this.pool, this.pool.Length + 1);
            this.pool[this.pool.Length - 1] = UnityEngine.Object.Instantiate(this.prefab);
            return this.pool[this.pool.Length - 1];
        }

        /// <summary>
        /// Get an object from the pool. Note that this immediately enables the game object in the scene.
        /// </summary>
        /// <returns></returns>
        public T GetObject() {
            for(int i = 0; i < this.pool.Length; i++) {
                if(!this.pool[i].gameObject.activeInHierarchy) {
                    this.pool[i].gameObject.SetActive(true);
                    return this.pool[i];
                }
            }

            //no object found, increase the pool by one
            Debug.LogWarningFormat("The object pool for the type {0} had to be resized. You should initiate this object pool with a larger size.", typeof(T).Name);
            Array.Resize<T>(ref this.pool, this.pool.Length + 1);
            this.pool[this.pool.Length - 1] = UnityEngine.Object.Instantiate(this.prefab);
            this.pool[this.pool.Length - 1].gameObject.SetActive(true);
            return this.pool[this.pool.Length - 1];
        }
    }
}

