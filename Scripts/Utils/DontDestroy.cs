﻿using UnityEngine;
namespace Jadoku.Utils {
    public class DontDestroy : MonoBehaviour {
        void Awake() {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}