﻿using Jadoku.SerializableDictionaries;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Jadoku.Utils {
    public class QualityByTeir : MonoBehaviour {

        [Header("Use this component to automatically set the quality level and other graphical settings based on the detected hardware teir." +
            "\n" +
            "\n" +
            "Note that the quality is an integer, where 0 is the quality level at the top of the quality settings")]
        public QualityByTeirSettingsDictionary settings = new QualityByTeirSettingsDictionary();

        private void Awake() {
            if(this.settings.ContainsKey(Graphics.activeTier)) {
                QualitySettings.SetQualityLevel(this.settings[Graphics.activeTier].qualityLevel);
                foreach(GameObject go in this.settings[Graphics.activeTier].disabledObjects) {
                    go.SetActive(false);
                }
            }
        }
    }

    [Serializable]
    public class QualityByTeirSettings {
        public int qualityLevel;
        public List<GameObject> disabledObjects = new List<GameObject>();
    }

    [Serializable]
    public class QualityByTeirSettingsDictionary : SerializableDictionaryBase<UnityEngine.Rendering.GraphicsTier, QualityByTeirSettings> { }
}
