﻿#if UNITY_WEBGL
using System.Runtime.InteropServices;

namespace Jadoku.Utils {
    public static class WebGLCookies {
        [DllImport("__Internal")]
        private static extern void WebGLCookies_Set(string name, string value, int days);

        [DllImport("__Internal")]
        private static extern string WebGLCookies_Get(string name);

        /// <summary>
        /// Set a cookie value
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public static void Set(string name, string value, int days) {
            WebGLCookies_Set(name, value, days);
        }

        /// <summary>
        /// Get a cookie. Returns "" if nothing found
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string Get(string name) {
            return WebGLCookies_Get(name);
        }
    }
}
#endif