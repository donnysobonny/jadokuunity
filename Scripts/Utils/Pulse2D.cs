﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Jadoku.Utils {
    public class Pulse2D : MonoBehaviour {

        public float minSize = 0.8f;
        public float maxSize = 1.2f;
        public float speed = 1f;

        public bool pulseOnStart = false;

        private void Start() {
            if(this.pulseOnStart) {
                this.StartPulse();
            }
        }

        public bool IsPulsing() {
            return this.pulsing;
        }

        bool pulsing;
        bool pulseIn = true;
        public void StartPulse() {
            this.pulsing = true;
            this.pulseIn = true;
        }
        public void StopPulse() {
            this.pulsing = false;
        }

        private void Update() {
            if(this.pulsing) {
                if(this.pulseIn) {
                    if(Mathf.Abs(this.transform.localScale.x - this.maxSize) <= 0.001f) {
                        this.pulseIn = false;
                    } else {
                        this.transform.localScale = Vector3.MoveTowards(this.transform.localScale, Vector3.one * this.maxSize, Time.deltaTime * this.speed);
                    }
                } else {
                    if(Mathf.Abs(this.transform.localScale.x - this.minSize) <= 0.001f) {
                        this.pulseIn = true;
                    } else {
                        this.transform.localScale = Vector3.MoveTowards(this.transform.localScale, Vector3.one * this.minSize, Time.deltaTime * this.speed);
                    }
                }
            } else {
                this.transform.localScale = Vector3.Lerp(this.transform.localScale, Vector3.one, Time.deltaTime * this.speed);
            }
        }
    }
}
