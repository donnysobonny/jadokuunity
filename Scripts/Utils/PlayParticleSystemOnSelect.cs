﻿using UnityEngine;

namespace Jadoku.Utils {
    [RequireComponent(typeof(ParticleSystem))]
    public class PlayParticleSystemOnSelect : MonoBehaviour {

        ParticleSystem ps;

        private void OnDrawGizmosSelected() {
            if(this.ps == null) {
                this.ps = this.GetComponent<ParticleSystem>();
            }
            if(!this.ps.isPlaying) {
                this.ps.Play();
            }
        }
    }
}