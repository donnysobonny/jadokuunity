﻿using System.Collections;
using UnityEngine;

namespace Jadoku.Utils {
    public class SceneSwitchOnStart : MonoBehaviour {
        public string switchToScene = "SampleScene";
        public float switchDelay = 3f;
        public UnityEngine.SceneManagement.LoadSceneMode switchSceneMode = UnityEngine.SceneManagement.LoadSceneMode.Single;

        private IEnumerator Start() {
            yield return new WaitForSeconds(this.switchDelay);
            UnityEngine.SceneManagement.SceneManager.LoadScene(this.switchToScene, this.switchSceneMode);
        }
    }
}