﻿using UnityEngine;

namespace Jadoku.Utils {
    [RequireComponent(typeof(Camera))]
    [ExecuteInEditMode]
    public class CameraSizeWithScreen : MonoBehaviour {
        public Camera cam;
        public float horizontalResolution = 1920;
        private void Awake() {
            if(this.cam == null) {
                this.cam = this.GetComponent<Camera>();
            }
            float currentAspect = (float)Screen.width / (float)Screen.height;
            this.cam.orthographicSize = horizontalResolution / currentAspect / 200;
        }
        
        private void Start() {
            float currentAspect = (float)Screen.width / (float)Screen.height;
            this.cam.orthographicSize = horizontalResolution / currentAspect / 200;
        }

        private void Update() {
            float currentAspect = (float)Screen.width / (float)Screen.height;
            this.cam.orthographicSize = horizontalResolution / currentAspect / 200;
        }
    }
}
