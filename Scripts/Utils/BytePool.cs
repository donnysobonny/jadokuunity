﻿namespace Jadoku.Utils {
    public class BytePool {
        byte value;

        public BytePool() { }
        public BytePool(byte start) {
            this.value = start;
        }

        public byte GetNext() {
            if(this.value >= byte.MaxValue) {
                this.value = 0;
            }
            this.value++;
            return this.value;
        }
    }
}
