﻿namespace Jadoku.Utils {
    public class UIntPool {
        uint value;

        public UIntPool() { }
        public UIntPool(uint start) {
            this.value = start;
        }

        public uint GetNext() {
            if(this.value >= uint.MaxValue) {
                this.value = 0;
            }
            this.value++;
            return this.value;
        }
    }
}
