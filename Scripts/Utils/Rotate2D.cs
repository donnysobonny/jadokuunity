﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jadoku.Utils {
    public class Rotate2D : MonoBehaviour {

        public float speed;

        // Update is called once per frame
        void Update() {
            this.transform.eulerAngles = new Vector3(
                this.transform.eulerAngles.x,
                this.transform.eulerAngles.y,
                this.transform.eulerAngles.z + (Time.deltaTime * this.speed)    
            );
        }
    }
}