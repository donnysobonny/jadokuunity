﻿using System;
using UnityEngine;

namespace Jadoku.Utils {
    [System.Serializable]
    public class BigNumber {
        public double value;
        public BigNumberExponent exponent;

        public BigNumber() {
            Recalculate();
        }
        public BigNumber(double value) {
            this.Set(value);
        }
        public BigNumber(double value, BigNumberExponent exponent) {
            this.value = value;
            this.exponent = exponent;
        }
        public BigNumber(BigNumber clone) {
            this.value = clone.value;
            this.exponent = clone.exponent;
        }
        public BigNumber(string numeric) {
            string[] parts = numeric.Split('.');
            //the length of the first part is used to decide the thousand exponential
            this.exponent = (BigNumberExponent)((int)Math.Floor((double)((parts[0].Length - 1) / 3)));
            //knowing the exponent, we simply need to add a dot exponent places from the right
            string format = parts[0].Substring(0, parts[0].Length - ((int)this.exponent * 3)) + "." + parts[0].Substring(parts[0].Length - ((int)this.exponent * 3), (int)this.exponent * 3);
            //if there was a dot, add the remainder on. Doubles can take any length of values after the dot
            if(parts.Length > 1) {
                format += parts[1];
            }
            //and parse
            this.value = double.Parse(format);
        }

        public BigNumber Clone() {
            return new BigNumber(this);
        }

        void Recalculate() {
            if(this.value >= 1000) {                
                this.IncreaseExponent();
            } else if(this.value < 1) {
                this.DecreaseExponent();
            }
        }
        void IncreaseExponent() {
            if(this.exponent != BigNumberExponent.zz) {
                this.exponent++;
                this.value /= 1000;
                this.Recalculate();
            }
        }
        void DecreaseExponent() {
            if(this.exponent != BigNumberExponent.zero) {
                this.exponent--;
                this.value *= 1000;
                this.Recalculate();
            }
        }

        double GetInternalValue(double value) {
            if(this.exponent > BigNumberExponent.zero) {
                return value / Math.Pow(1000, (int)this.exponent);
            }
            return value;
        }

        double GetExternalValue(double value) {
            if(this.exponent > BigNumberExponent.zero) {
                return value * Math.Pow(1000, (int)this.exponent);
            }
            return value;
        }

        public double GetInternalValue(BigNumber bigNumber) {
            if(bigNumber.exponent > this.exponent) {
                //need to raise
                return bigNumber.value * Math.Pow(1000, ((int)bigNumber.exponent - (int)this.exponent));
            }
            if(bigNumber.exponent < this.exponent) {
                //need to lower
                return bigNumber.value / Math.Pow(1000, ((int)this.exponent - (int)bigNumber.exponent));
            }
            //same exponents
            return bigNumber.value;
        }

        public double GetExternalValue(BigNumber bigNumber) {
            if(bigNumber.exponent > this.exponent) {
                return bigNumber.value / Math.Pow(1000, ((int)bigNumber.exponent - (int)this.exponent));
            }
            if(bigNumber.exponent < this.exponent) {
                return bigNumber.value * Math.Pow(1000, ((int)this.exponent - (int)bigNumber.exponent));
            }
            //same exponents
            return bigNumber.value;
        }

        public static BigNumber operator +(BigNumber l, double r) {
            BigNumber nl = new BigNumber(l);
            nl.Add(r);
            return nl;
        }
        public static BigNumber operator -(BigNumber l, double r) {
            BigNumber nl = new BigNumber(l);
            nl.Subtract(r);
            return nl;
        }
        public static BigNumber operator -(double l, BigNumber r) {
            BigNumber nl = new BigNumber(l);
            nl.Subtract(r);
            return nl;
        }
        public static BigNumber operator *(BigNumber l, double r) {
            BigNumber nl = new BigNumber(l);
            nl.Multiply(r);
            return nl;
        }
        public static BigNumber operator /(BigNumber l, double r) {
            BigNumber nl = new BigNumber(l);
            nl.Devide(r);
            return nl;
        }
        public static BigNumber operator /(double l, BigNumber r) {
            BigNumber nl = new BigNumber(l);
            nl.Devide(r);
            return nl;
        }

        public static BigNumber operator +(BigNumber l, BigNumber r) {
            BigNumber nl = new BigNumber(l);
            nl.Add(r);
            return nl;
        }
        public static BigNumber operator -(BigNumber l, BigNumber r) {
            BigNumber nl = new BigNumber(l);
            nl.Subtract(r);
            return nl;
        }
        public static BigNumber operator *(BigNumber l, BigNumber r) {
            BigNumber nl = new BigNumber(l);
            nl.Multiply(r);
            return nl;
        }
        public static BigNumber operator /(BigNumber l, BigNumber r) {
            BigNumber nl = new BigNumber(l);
            nl.Devide(r);
            return nl;
        }

        public BigNumber Set(double value) {
            this.value = value;
            this.exponent = BigNumberExponent.zero;
            this.Recalculate();
            return this;
        }
        public BigNumber Set(BigNumber value) {
            this.value = value.value;
            this.exponent = value.exponent;
            return this;
        }

        public BigNumber Add(double value) {
            this.value += this.GetInternalValue(value);
            this.Recalculate();
            return this;
        }
        public BigNumber Add(BigNumber bigNumber) {
            this.value += this.GetInternalValue(bigNumber);
            this.Recalculate();
            return this;
        }

        public BigNumber Subtract(double value) {
            this.value -= this.GetInternalValue(value);
            this.Recalculate();
            return this;
        }
        public BigNumber Subtract(BigNumber value) {
            this.value -= this.GetInternalValue(value);
            this.Recalculate();
            return this;
        }

        public BigNumber SubractBy(double value) {
            this.value = this.GetInternalValue(value) - this.value;
            this.Recalculate();
            return this;
        }
        public BigNumber SubractBy(BigNumber value) {
            this.value = this.GetInternalValue(value) - this.value;
            this.Recalculate();
            return this;
        }

        public BigNumber Multiply(double value) {
            this.value *= value;
            this.Recalculate();
            return this;
        }
        public BigNumber Multiply(BigNumber value) {
            this.value *= this.GetExternalValue(this.GetInternalValue(value));
            this.Recalculate();
            return this;
        }

        public BigNumber Devide(double value) {
            this.value /= value;
            this.Recalculate();
            return this;
        }
        public BigNumber Devide(BigNumber value) {
            this.value /= this.GetExternalValue(this.GetInternalValue(value));
            this.Recalculate();
            return this;
        }

        public BigNumber DevideBy(double value) {
            this.value = value / this.value;
            this.Recalculate();
            return this;
        }
        public BigNumber DevideBy(BigNumber value) {
            this.value = this.GetInternalValue(value) / this.value;
            this.Recalculate();
            return this;
        }

        public BigNumber Raise(double power) {
            this.value = Math.Pow(this.value, power);
            this.Recalculate();
            return this;
        }

        public static bool operator ==(BigNumber l, BigNumber r) {
            return l.exponent == r.exponent && l.value == r.value;
        }
        public static bool operator !=(BigNumber l, BigNumber r) {
            return l.exponent != r.exponent || l.value != r.value;
        }
        public static bool operator >(BigNumber l, BigNumber r) {
            return l.value > l.GetInternalValue(r);
        }
        public static bool operator <(BigNumber l, BigNumber r) {
            return l.value < l.GetInternalValue(r);
        }
        public static bool operator <=(BigNumber l, BigNumber r) {
            return l.value <= l.GetInternalValue(r);
        }
        public static bool operator >=(BigNumber l, BigNumber r) {
            return l.value >= l.GetInternalValue(r);
        }

        public static bool operator ==(BigNumber l, double r) {
            return l.GetInternalValue(r) == l.value;
        }
        public static bool operator !=(BigNumber l, double r) {
            return l.GetInternalValue(r) != l.value;
        }
        public static bool operator >(BigNumber l, double r) {
            return l.value > l.GetInternalValue(r);
        }
        public static bool operator <(BigNumber l, double r) {
            return l.value < l.GetInternalValue(r);
        }
        public static bool operator <=(BigNumber l, double r) {
            return l.value <= l.GetInternalValue(r);
        }
        public static bool operator >=(BigNumber l, double r) {
            return l.value >= l.GetInternalValue(r);
        }

        /// <summary>
        /// Get a string representation of the number, to a decimal point precision of 1
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            return this.ToString(1);
        }

        /// <summary>
        /// Get a string representation of the number, to a certain number of decimal points for larger numbers
        /// </summary>
        /// <param name="decimalPlaces"></param>
        /// <returns></returns>
        public string ToString(int decimalPlaces) {
            if(this.exponent == BigNumberExponent.zero) {
                return string.Format("{0:n0}", this.value);
            }
            if(this.exponent == BigNumberExponent.thousand) {
                return string.Format("{0:n0}", this.value * 1000);
            }
            return string.Format("{0:n" + decimalPlaces + "}", this.value) + BigNumber.GetExponentShort(this.exponent);
        }

        /// <summary>
        /// Get a string representation of the number, and smartly choose the decimal point precision based on the size of the number
        /// </summary>
        /// <returns></returns>
        public string ToStringSmart() {
            if(this.exponent == BigNumberExponent.zero) {
                return string.Format("{0:n0}", this.value);
            }
            if(this.exponent == BigNumberExponent.thousand) {
                return string.Format("{0:n0}", this.value * 1000);
            }
            if(this.value >= 100) {
                return string.Format("{0:n1}", this.value) + BigNumber.GetExponentShort(this.exponent);
            } else if(this.value >= 10) {
                return string.Format("{0:n2}", this.value) + BigNumber.GetExponentShort(this.exponent);
            }
            return string.Format("{0:n3}", this.value) + BigNumber.GetExponentShort(this.exponent);
        }

        /// <summary>
        /// Get the real value as a double
        /// 
        /// WARNING! If the real value is more than double.MaxValue, then double.MaxValue will be returned. 
        /// Only deal with the real value when you are certain that it will be a smaller number
        /// </summary>
        /// <returns></returns>
        public double ToDouble() {
            return this.value * Math.Pow(1000, (int)this.exponent);
        }

        /// <summary>
        /// Get the real value as a float
        /// 
        /// WARNING! If the real value is more than float.MaxValue, then float.MaxValue will be returned. 
        /// Only deal with the real value when you are certain that it will be a smaller number
        /// </summary>
        /// <returns></returns>
        public float ToFloat() {
            return Convert.ToSingle(this.ToDouble());
        }

        public static string GetExponentShort(BigNumberExponent exponent) {
            if(exponent <= BigNumberExponent.thousand) {
                return "";
            } else if(exponent <= BigNumberExponent.trillion) {
                return exponent.ToString().Substring(0, 1).ToUpper();
            } else {
                return exponent.ToString();
            }
        }

        public override bool Equals(object obj) {
            var number = obj as BigNumber;
            return number != null &&
                   this.value == number.value &&
                   this.exponent == number.exponent;
        }

        public override int GetHashCode() {
            var hashCode = -729427552;
            hashCode = hashCode * -1521134295 + this.value.GetHashCode();
            hashCode = hashCode * -1521134295 + this.exponent.GetHashCode();
            return hashCode;
        }
    }

    public enum BigNumberExponent : int {
        zero,
        thousand,
        million,
        billion,
        trillion,
        a,
        b,
        c,
        d,
        e,
        f,
        g,
        h,
        i,
        j,
        k,
        l,
        m,
        n,
        o,
        p,
        q,
        r,
        s,
        t,
        u,
        v,
        w,
        x,
        y,
        z,
        aa,
        bb,
        cc,
        dd,
        ee,
        ff,
        gg,
        hh,
        ii,
        jj,
        kk,
        ll,
        mm,
        nn,
        oo,
        pp,
        qq,
        rr,
        ss,
        tt,
        uu,
        vv,
        ww,
        xx,
        yy,
        zz
    }
}
