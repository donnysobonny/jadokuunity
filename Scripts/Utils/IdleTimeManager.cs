using System;
using UnityEngine;
using UnityEngine.Events;

namespace Jadoku.Utils {
    /// <summary>
    /// A component that makes it easy to handle idle time (how many seconds the game was idle)
    /// </summary>
    public class IdleTimeManager : MonoBehaviour {
        public static IdleTimeManager instance { get; private set; }
        private void Awake() {
            if(instance != null) {
                Debug.LogError("You have more than one IdleTimeManager in the scene. There should be a maximum of one");
            }
            instance = this;
        }       

        [Tooltip("The minimum number of seconds that the game must be idle for the event to trigger, or for GetSecondsPassedSinceLastIdle to return a value")]
        public int minimumIdleSeconds = 120;
        [Tooltip("The maximum number of seconds that will be returned by GetSecondsPassedSinceLastIdle or be passed to the event. Default is 14 days")]
        public int maximumIdleSeconds = 60 * 60 * 24 * 14;
        [Tooltip("This event is triggered when the game starts or retains focus, and passes the number of seconds that has passed")]
        public IdleTimeManagerTimeEvent onReturnOrStartup;

        /// <summary>
        /// Get the number of seconds that have passed since the game last went idle. If the seconds is less than the minimum, 0 is returned. If it is more than the maximum, the maximum is returned
        /// </summary>
        /// <returns></returns>
        public int GetSecondsPassedSinceLastIdle() {
            if(PlayerPrefs.HasKey("idle_time_manager_out_time")) {
                DateTime outTime = DateTime.FromBinary(long.Parse(PlayerPrefs.GetString("idle_time_manager_out_time")));
                double seconds = (DateTime.Now - outTime).TotalSeconds;
                if(seconds < this.minimumIdleSeconds) {
                    return 0;
                }
                if(seconds > this.maximumIdleSeconds) {
                    seconds = this.maximumIdleSeconds;
                }
                return Mathf.RoundToInt(Convert.ToSingle(seconds));
            } else {
                return 0;
            }
        }

        private void Start() {
            this.HandleGameIn();
        }

        private void OnApplicationQuit() {
            this.HandleGameOut();
        }

        private void OnApplicationFocus(bool focus) {
            if(focus) {
                this.HandleGameIn();
            } else {
                this.HandleGameOut();
            }
        }

        private void HandleGameIn() {
            int seconds = this.GetSecondsPassedSinceLastIdle();
            if(seconds > 0) {
                this.onReturnOrStartup.Invoke(seconds);
            }
        }

        private void HandleGameOut() {
            PlayerPrefs.SetString("idle_time_manager_out_time", DateTime.Now.ToBinary().ToString());
            PlayerPrefs.Save();
        }
    }
}

[Serializable]
public class IdleTimeManagerTimeEvent : UnityEvent<int> { }