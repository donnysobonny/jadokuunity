﻿namespace Jadoku.Utils {
    public class UShortPool {
        ushort value;

        public UShortPool() { }
        public UShortPool(ushort start) {
            this.value = start;
        }

        public ushort GetNext() {
            if(this.value >= ushort.MaxValue) {
                this.value = 0;
            }
            this.value++;
            return this.value;
        }
    }
}
