﻿using UnityEngine;

namespace Jadoku.Utils {
    public class ScrollUV : MonoBehaviour {

        public SpriteRenderer spriteRenderer;
        public Vector2 speed;

        // Update is called once per frame
        void Update() {
            this.spriteRenderer.material.mainTextureOffset += this.speed * Time.deltaTime;
        }
    }
}