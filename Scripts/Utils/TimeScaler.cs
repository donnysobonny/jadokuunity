﻿using System;
using UnityEngine;

namespace Jadoku.Utils {
    /// <summary>
    /// A utility component used to manage changes to the time scale
    /// </summary>
    public class TimeScaler : MonoBehaviour {
        public static TimeScaler instance { get; private set; }
        private void Awake() {
            if(instance != null) {
                Debug.LogError("You have more than one TimeScaler component in the scene. There should only ever be one per scene!");
            }
            instance = this;
        }

        public float defaultTimeScale = 1f;
        public float defaultChangeSpeed = 5f;
        public TimeScalerUpdateType defaultUpdateType = TimeScalerUpdateType.movetowards;

        private Action onDone;
        private bool scaling;
        private float target = 1f;
        private float changeSpeed = 0f;
        private TimeScalerUpdateType updateType = TimeScalerUpdateType.movetowards;

        /// <summary>
        /// Check if the time is currenctly being scaled
        /// </summary>
        /// <returns></returns>
        public bool IsScaling() {
            return this.scaling;
        }
        
        /// <summary>
        /// Stop any scaling being applied by the time scaler
        /// </summary>
        public void StopScaling() {
            this.scaling = false;
        }
        /// <summary>
        /// Resume any scaling applied by the time scaler
        /// </summary>
        public void StartScaling() {
            this.scaling = true;
        }

        /// <summary>
        /// Set the time scale immediately
        /// </summary>
        /// <param name="timeScale"></param>
        public void SetTimeScale(float timeScale) {
            Time.timeScale = this.target = timeScale;
            this.scaling = false;
            this.onDone = null;
        }

        /// <summary>
        /// Change the time scale over time
        /// </summary>
        /// <param name="targetTimeScale"></param>
        /// <param name="changeSpeed"></param>
        /// <param name="updateType"></param>
        /// <param name="onDone"></param>
        public void ChangeTimeScale(float targetTimeScale, float changeSpeed, TimeScalerUpdateType updateType, Action onDone) {
            this.target = targetTimeScale;
            this.changeSpeed = changeSpeed;
            this.updateType = updateType;
            if(onDone != null) {
                this.onDone = onDone;
            }
            this.scaling = true;
        }
        /// <summary>
        /// Change the time scale over time
        /// </summary>
        /// <param name="targetTimeScale"></param>
        /// <param name="onDone"></param>
        public void ChangeTimeScale(float targetTimeScale, Action onDone = null) {
            this.ChangeTimeScale(targetTimeScale, this.defaultChangeSpeed, this.defaultUpdateType, onDone);
        }
        /// <summary>
        /// Change the time scale over time
        /// </summary>
        /// <param name="targetTimeScale"></param>
        /// <param name="changeSpeed"></param>
        /// <param name="onDone"></param>
        public void ChangeTimeScale(float targetTimeScale, float changeSpeed, Action onDone = null) {
            this.ChangeTimeScale(targetTimeScale, changeSpeed, this.defaultUpdateType, onDone);
        }
        /// <summary>
        /// Change the time scale over time
        /// </summary>
        /// <param name="targetTimeScale"></param>
        /// <param name="updateType"></param>
        /// <param name="onDone"></param>
        public void ChangeTimeScale(float targetTimeScale, TimeScalerUpdateType updateType, Action onDone = null) {
            this.ChangeTimeScale(targetTimeScale, this.defaultChangeSpeed, updateType, onDone);
        }

        /// <summary>
        /// Reset the time scale to 1 over time
        /// </summary>
        /// <param name="changeSpeed"></param>
        /// <param name="updateType"></param>
        /// <param name="onDone"></param>
        public void ResetTimeScale(float changeSpeed, TimeScalerUpdateType updateType, Action onDone) {
            this.ChangeTimeScale(this.defaultTimeScale, changeSpeed, updateType, onDone);
        }
        /// <summary>
        /// Reset the time scale to 1 over time
        /// </summary>
        /// <param name="onDone"></param>
        public void ResetTimeScale(Action onDone = null) {
            this.ResetTimeScale(this.defaultChangeSpeed, this.defaultUpdateType, onDone);
        }
        /// <summary>
        /// Reset the time scale to 1 over time
        /// </summary>
        /// <param name="changeSpeed"></param>
        /// <param name="onDone"></param>
        public void ResetTimeScale(float changeSpeed, Action onDone = null) {
            this.ResetTimeScale(changeSpeed, this.defaultUpdateType, onDone);
        }
        /// <summary>
        /// Reset the time scale to 1 over time
        /// </summary>
        /// <param name="updateType"></param>
        /// <param name="onDone"></param>
        public void ResetTimeScale(TimeScalerUpdateType updateType, Action onDone = null) {
            this.ResetTimeScale(this.defaultChangeSpeed, updateType, onDone);
        }

        /// <summary>
        /// Change the time scale and then reset it back to 1 over time
        /// </summary>
        /// <param name="targetTimeScale"></param>
        /// <param name="changeSpeed"></param>
        /// <param name="changeUpdateType"></param>
        /// <param name="resetSpeed"></param>
        /// <param name="resetUpdateType"></param>
        /// <param name="onDone"></param>
        public void ChangeAndResetTimeScale(
            float targetTimeScale,
            float changeSpeed,
            TimeScalerUpdateType changeUpdateType,
            float resetSpeed,
            TimeScalerUpdateType resetUpdateType,
            Action onDone
        ) {
            this.ChangeTimeScale(targetTimeScale, changeSpeed, changeUpdateType, () => {
                this.ResetTimeScale(resetSpeed, resetUpdateType, onDone);
            });
        }
        /// <summary>
        /// Change the time scale and then reset it back to 1 over time
        /// </summary>
        /// <param name="targetTimeScale"></param>
        /// <param name="changeSpeed"></param>
        /// <param name="updateType"></param>
        /// <param name="onDone"></param>
        public void ChangeAndResetTimeScale(float targetTimeScale, float changeSpeed, TimeScalerUpdateType updateType, Action onDone) {
            this.ChangeAndResetTimeScale(targetTimeScale, changeSpeed, updateType, changeSpeed, updateType, onDone);
        }
        /// <summary>
        /// Change the time scale and then reset it back to 1 over time
        /// </summary>
        /// <param name="targetTimeScale"></param>
        /// <param name="onDone"></param>
        public void ChangeAndResetTimeScale(float targetTimeScale, Action onDone = null) {
            this.ChangeAndResetTimeScale(targetTimeScale, this.defaultChangeSpeed, this.defaultUpdateType, onDone);
        }
        /// <summary>
        /// Change the time scale and then reset it back to 1 over time
        /// </summary>
        /// <param name="targetTimeScale"></param>
        /// <param name="changeSpeed"></param>
        /// <param name="onDone"></param>
        public void ChangeAndResetTimeScale(float targetTimeScale, float changeSpeed, Action onDone = null) {
            this.ChangeAndResetTimeScale(targetTimeScale, changeSpeed, this.defaultUpdateType, onDone);
        }
        /// <summary>
        /// Change the time scale and then reset it back to 1 over time
        /// </summary>
        /// <param name="targetTimeScale"></param>
        /// <param name="updateType"></param>
        /// <param name="onDone"></param>
        public void ChangeAndResetTimeScale(float targetTimeScale, TimeScalerUpdateType updateType, Action onDone = null) {
            this.ChangeAndResetTimeScale(targetTimeScale, this.defaultChangeSpeed, updateType, onDone);
        }

        private void Start() {
            this.SetTimeScale(this.defaultTimeScale);
        }

        private void Update() {
            if(this.scaling) {
                if(Mathf.Abs(Time.timeScale - this.target) > 0.00001f) {
                    switch(this.updateType) {
                        case TimeScalerUpdateType.lerp:
                            Time.timeScale = Mathf.Lerp(Time.timeScale, this.target, Time.unscaledDeltaTime * this.changeSpeed);
                            break;
                        case TimeScalerUpdateType.movetowards:
                            Time.timeScale = Mathf.MoveTowards(Time.timeScale, this.target, Time.unscaledDeltaTime * this.changeSpeed);
                            break;
                    }
                } else {
                    this.scaling = false;
                    if(this.onDone != null) {
                        this.onDone();
                    }
                    this.onDone = null;
                }
            }
        }
    }

    public enum TimeScalerUpdateType {
        lerp,
        movetowards
    }
}