﻿using UnityEngine;

namespace Jadoku.Utils {
    public class Follow : MonoBehaviour {
        public Transform target;
        public Vector3 offset;
        void Update() {
            if(this.target != null) {
                this.transform.position = this.target.position + this.offset;
            }
        }
    }
}
