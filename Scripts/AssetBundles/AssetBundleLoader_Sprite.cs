﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Jadoku.AssetBundles {
    public class AssetBundleLoader_Sprite : IAssetBundleLoader {
        public SpriteRenderer[] spriteRenderers;
        public Image[] images;

        public override void LoadAsset(Action onLoad = null) {
            AssetBundleManager.LoadAsset<Sprite>(this.bundleName, this.assetName, delegate (Sprite sprite) {
                if(this.spriteRenderers != null) {
                    foreach(SpriteRenderer sr in this.spriteRenderers) {
                        sr.sprite = sprite;
                    }
                }
                if(this.images != null) {
                    foreach(Image image in this.images) {
                        image.sprite = sprite;
                    }
                }
                if(onLoad != null) {
                    onLoad();
                }
            }, null, delegate(string error) {
                this.Error(error);
            }, this.cacheAssetInMemory);
        }
    }
}
