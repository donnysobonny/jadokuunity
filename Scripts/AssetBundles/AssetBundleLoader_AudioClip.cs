﻿using System;
using UnityEngine;

namespace Jadoku.AssetBundles {
    public class AssetBundleLoader_AudioClip : IAssetBundleLoader {
        public AudioSource[] audioSources;

        public bool playOnLoad = false;

        public override void LoadAsset(Action onLoad = null) {
            AssetBundleManager.LoadAsset<AudioClip>(this.bundleName, this.assetName, delegate (AudioClip clip) {
                if(this.audioSources != null) {
                    foreach(AudioSource s in this.audioSources) {
                        s.clip = clip;
                        if(this.playOnLoad) {
                            s.Play();
                        }
                    }
                    if(onLoad != null) {
                        onLoad();
                    }
                }
            }, null, delegate (string error) {
                this.Error(error);
            }, this.cacheAssetInMemory);
        }
    }
}