﻿using Jadoku.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jadoku.AssetBundles {
    public class AssetBundleManager : MonoBehaviour {
        static AssetBundleManager instance;
        string platformSlug;
        private void Awake() {
            instance = this;
#if UNITY_STANDALONE_WIN
            this.platformSlug = "win";
#elif UNITY_STANDALONE_OSX
            this.platformSlug = "osx";
#elif UNITY_STANDALONE_LINUX
            this.platformSlug = "linux";
#elif UNITY_IOS
            this.platformSlug = "ios";
#elif UNITY_ANDROID
            this.platformSlug = "android";
#elif UNITY_WEBGL
            this.platformSlug = "webgl";
#endif
        }
        
        [Tooltip("The root location where asset bundles can be accessed")]
        public string assetBundleLocation;
        [Tooltip("Enable this if you want to use a json file in order to specify the versioned asset bundles, rather than the compiled game data")]
        public bool useJsonSchema = false;
        [Tooltip("The url to the json schema file. This file must be correctly formatted!")]
        public string jsonSchemaUrl = "";

        public VersionedAssetBundle[] versionedAssetBundles;
        List<string> bundlesBeingLoaded = new List<string>();

        Dictionary<string, AssetBundle> cachedAssetBundles = new Dictionary<string, AssetBundle>();
        Dictionary<string, UnityEngine.Object> cachedObjects = new Dictionary<string, UnityEngine.Object>();

        bool jsonSchemaDownloaded = false;
        bool jsonSchemaDownloading = false;
        void DownloadJsonSchemaIfNeeded(Action<string> onError = null) {
            if(this.jsonSchemaDownloading) {
                return;
            }
            this.jsonSchemaDownloading = true;
            this.StartCoroutine(this.DownloadJsonSchemaIfNeeded_Request(new WWW(this.jsonSchemaUrl), onError));
        }
        IEnumerator DownloadJsonSchemaIfNeeded_Request(WWW req, Action<string> onError = null) {
            yield return req;

            if(!string.IsNullOrEmpty(req.error)) {
                Debug.LogErrorFormat("ASSETBUNDLEMANAGER failed to download json schema: {0}  \n\n {1}", req.error, req.text);
                if(onError != null) {
                    onError("failed to download json schema: " + req.error);
                }
            } else {
                try {
                    this.versionedAssetBundles = JsonUtility.FromJson<VersionedAssetBundle[]>(req.text);

                    this.jsonSchemaDownloaded = true;
                    this.jsonSchemaDownloading = false;
                } catch(Exception ex) {
                    Debug.LogErrorFormat("ASSETBUNDLEMANAGER failed to decode json schema: {0} \n\n {1}", ex.Message, req.text);
                    if(onError != null) {
                        onError("failed to decode json schema: " + ex.Message);
                    }
                }
            }
        }

        public static void LoadAssetBundle(string name, Action onComplete, Action<float> onProgress = null, Action<string> onError = null) {
            instance.StartCoroutine(instance._LoadAssetBundle(name, onComplete, onProgress, onError));
        }
        IEnumerator _LoadAssetBundle(string name, Action onComplete, Action<float> onProgress = null, Action<string> onError = null) {
            if(this.useJsonSchema) {
                this.DownloadJsonSchemaIfNeeded(onError);
                while(this.jsonSchemaDownloading) {
                    yield return null;
                }
                if(!this.jsonSchemaDownloaded) {
                    yield break;
                }
            }

            VersionedAssetBundle b = this.GetVersionedAssetBundleByName(name);
            if(b == null) {
                Debug.LogErrorFormat("ASSETBUNDLEMANAGER could not find a versioned asset bundle with name {0}", name);
                if(onError != null) {
                    onError("could not find a versioned asset bundle by the name of " + name);
                }
                yield break;
            }
            string url = this.assetBundleLocation + "/" + b.version + "/" + this.platformSlug + "/" + b.name;

            while(!Caching.ready) {
                //caching not ready yet...
                yield return null;
            }

            //if this asset bundle is already loading
            while(this.bundlesBeingLoaded.Contains(name)) {
                yield return null;
            }

            if(this.cachedAssetBundles.ContainsKey(name)) {
                if(onProgress != null) {
                    onProgress(1f);
                }
                if(onComplete != null) {
                    onComplete();
                }
                yield break;
            } else {
                this.bundlesBeingLoaded.Add(name);

                WWW req = WWW.LoadFromCacheOrDownload(url, b.version);
                while(!req.isDone) {
                    if(onProgress != null) {
                        onProgress(req.progress);
                    }
                    yield return null;
                }

                if(!string.IsNullOrEmpty(req.error)) {
                    Debug.LogErrorFormat("ASSETBUNDLEMANAGER Failed to download asset bundle {0}: {1}", url, req.error);
                    if(onError != null) {
                        onError(req.error);
                    }
                } else {
                    if(req.assetBundle != null) {
                        //store it
                        this.cachedAssetBundles[name] = req.assetBundle;
                        //complete
                        if(onComplete != null) {
                            onComplete();
                        }
                    } else {
                        Debug.LogErrorFormat("ASSETBUNDLEMANAGER failed to get asset bundle from " + url);
                        if(onError != null) {
                            onError("did not get an asset bundle from " + url);
                        }
                    }
                }

                this.bundlesBeingLoaded.Remove(name);
            }
        }

        public static void LoadAsset<T>(string bundleName, string assetName, Action<T> onComplete, Action<float> onProgress = null, Action<string> onError = null, bool cacheAssetInMemory = true) where T : UnityEngine.Object {
            if(instance.cachedObjects.ContainsKey(bundleName + "." + assetName)) {
                if(onProgress != null) {
                    onProgress(1f);
                }
                if(onComplete != null) {
                    onComplete((T)instance.cachedObjects[bundleName + "." + assetName]);
                }
            } else {
                LoadAssetBundle(bundleName, delegate () {
                    try {
                        T obj = instance.cachedAssetBundles[bundleName].LoadAsset(assetName, typeof(T)) as T;
                        if(obj == null) {
                            Debug.LogErrorFormat("ASSETBUNDLEMANAGER could not load the asset {0} from the bundle {1}", assetName, bundleName);
                            if(onError != null) {
                                onError("Could not load the asset " + assetName + " from the bundle " + bundleName);
                            }
                        } else {
                            if(cacheAssetInMemory) {
                                instance.cachedObjects[bundleName + "." + assetName] = obj;
                            }
                            if(onComplete != null) {
                                onComplete((T)obj);
                            }
                        }
                    } catch(Exception ex) {
                        Debug.LogErrorFormat("ASSETBUNDLEMANAGER could not load the asset {0} from the bundle {1}: {2}", assetName, bundleName, ex.Message);
                        if(onError != null) {
                            onError("Could not load the asset " + assetName + " from the bundle " + bundleName);
                        }
                    }                    
                }, onProgress, onError);
            }
        }

        VersionedAssetBundle GetVersionedAssetBundleByName(string name) {
            foreach(VersionedAssetBundle b in this.versionedAssetBundles) {
                if(b.name == name) {
                    return b;
                }
            }
            return null;
        }

        /// <summary>
        /// Unload all objects and asset bundles from memory
        /// </summary>
        public static void Unload() {
            AssetBundleManager.instance._Unload();
        }
        void _Unload() {
            foreach(KeyValuePair<string, AssetBundle> pair in this.cachedAssetBundles) {
                pair.Value.Unload(true);
            }
            this.cachedAssetBundles.Clear();
        }

        private void OnApplicationQuit() {
            this._Unload();
        }

        private void OnDisable() {
            this._Unload();
        }
    }
}