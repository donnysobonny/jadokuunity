﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jadoku.AssetBundles {
    public abstract class IAssetBundleLoader : MonoBehaviour {
        public bool loadOnStart = false;
        public bool loadOnEnable = false;
        public string bundleName = "";
        public string assetName = "";
        public bool cacheAssetInMemory = true;

        void Start() {
            if(this.loadOnStart) {
                this.LoadAsset();
            }
        }
        void OnEnable() {
            if(this.loadOnEnable) {
                this.LoadAsset();
            }    
        }

        public abstract void LoadAsset(Action onLoad = null);

        protected void Error(string error) {
            Debug.LogErrorFormat("ASSETBUNDLELOADER failed to load asset {0} from bundle {1}: {2}", this.assetName, this.bundleName, error);
        }
    }
}