﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Jadoku.AssetBundles {
    public class AssetBundleLoader_Font : IAssetBundleLoader {
        public Text[] texts;

        public override void LoadAsset(Action onLoad = null) {
            AssetBundleManager.LoadAsset<Font>(this.bundleName, this.assetName, delegate (Font font) {
                if(this.texts != null) {
                    foreach(Text t in this.texts) {
                        t.font = font;
                    }
                }
                if(onLoad != null) {
                    onLoad();
                }
            }, null, delegate (string error) {
                this.Error(error);
            }, this.cacheAssetInMemory);
        }
    }
}
