﻿using UnityEngine;

namespace Jadoku.UI {
    [RequireComponent(typeof(CanvasGroup))]
    public class FloatingObject : MonoBehaviour {
        FloatingObjectQueue queue;
        FloatingObjectQueueDirection direction;

        public CanvasGroup canvasGroup;
        float startAlpha = 1f;

        float delay = 0f;
        float lifeTime = 0f;
        
        Vector3 toObjectStart;

        public void Init(FloatingObjectQueue queue, FloatingObjectQueueDirection direction, Vector3 position, Quaternion rotation, float alpha, bool localSpace) {
            this.queue = queue;
            this.direction = direction;
            if(localSpace) {
                this.transform.localPosition = position;
                this.transform.localRotation = rotation;
            } else {
                this.transform.position = position;
                this.transform.rotation = rotation;
            }
            this.delay = this.queue.lifeTimeDelay;
            this.lifeTime = 0f;
            this.canvasGroup.alpha = this.startAlpha = alpha;
            this.transform.localScale = Vector3.one;
            this.gameObject.SetActive(true);
        }

        private void Awake() {
            if(this.canvasGroup == null) {
                Debug.LogError("Floating object does not have canvas group assigned!");
            }
        }

        Vector3 initial, final;
        void Update() {
            if(!this.queue.floatToObject || this.delay > 0f) {
                switch(this.direction) {
                    case FloatingObjectQueueDirection.up:
                        this.transform.position += new Vector3(0f, 1f, 0f) * Time.deltaTime * this.queue.speed;
                        break;
                    case FloatingObjectQueueDirection.down:
                        this.transform.position += new Vector3(0f, -1f, 0f) * Time.deltaTime * this.queue.speed;
                        break;
                    case FloatingObjectQueueDirection.left:
                        this.transform.position += new Vector3(-1f, 0f, 0f) * Time.deltaTime * this.queue.speed;
                        break;
                    case FloatingObjectQueueDirection.right:
                        this.transform.position += new Vector3(1f, 0f, 0f) * Time.deltaTime * this.queue.speed;
                        break;
                    case FloatingObjectQueueDirection.upRight:
                        this.transform.position += new Vector3(0.5f, 0.5f, 0f) * Time.deltaTime * this.queue.speed;
                        break;
                    case FloatingObjectQueueDirection.upLeft:
                        this.transform.position += new Vector3(-0.5f, 0.5f, 0f) * Time.deltaTime * this.queue.speed;
                        break;
                    case FloatingObjectQueueDirection.downRight:
                        this.transform.position += new Vector3(0.5f, -0.5f, 0f) * Time.deltaTime * this.queue.speed;
                        break;
                    case FloatingObjectQueueDirection.downLeft:
                        this.transform.position += new Vector3(-0.5f, -0.5f, 0f) * Time.deltaTime * this.queue.speed;
                        break;
                    case FloatingObjectQueueDirection.bounceUpRight:
                        initial = new Vector3(0f, 1f, 0f) * (1f - (this.lifeTime / this.queue.lifeTime));
                        final = new Vector3(this.queue.horizontalBounceSpeed, 0f, 0f) * (this.lifeTime / this.queue.lifeTime);
                        this.transform.position += (initial + final) * Time.deltaTime * this.queue.speed;
                        break;
                    case FloatingObjectQueueDirection.bounceUpLeft:
                        initial = new Vector3(0f, 1f, 0f) * (1f - (this.lifeTime / this.queue.lifeTime));
                        final = new Vector3(-this.queue.horizontalBounceSpeed, 0f, 0f) * (this.lifeTime / this.queue.lifeTime);
                        this.transform.position += (initial + final) * Time.deltaTime * this.queue.speed;
                        break;
                    case FloatingObjectQueueDirection.bounceDownRight:
                        initial = new Vector3(0f, -1f, 0f) * (1f - (this.lifeTime / this.queue.lifeTime));
                        final = new Vector3(this.queue.horizontalBounceSpeed, 0f, 0f) * (this.lifeTime / this.queue.lifeTime);
                        this.transform.position += (initial + final) * Time.deltaTime * this.queue.speed;
                        break;
                    case FloatingObjectQueueDirection.bounceDownLeft:
                        initial = new Vector3(0f, -1f, 0f) * (1f - (this.lifeTime / this.queue.lifeTime));
                        final = new Vector3(-this.queue.horizontalBounceSpeed, 0f, 0f) * (this.lifeTime / this.queue.lifeTime);
                        this.transform.position += (initial + final) * Time.deltaTime * this.queue.speed;
                        break;
                }
            }            
        
            if(this.delay <= 0f) {
                switch(this.queue.fade) {
                    case FloatingObjectQueueFade.fade:
                        float alpha = 1f - (this.lifeTime / this.queue.lifeTime);
                        alpha *= this.startAlpha;
                        if(alpha > 0f) {
                            this.canvasGroup.alpha = alpha;
                        } else {
                            this.gameObject.SetActive(false);
                        }
                        break;
                    case FloatingObjectQueueFade.shrink:
                        float size = 1f - (this.lifeTime / this.queue.lifeTime);

                        if(this.queue.floatToObject) {
                            this.transform.position = this.toObjectStart + ((this.queue.toObject.position - this.toObjectStart).normalized * ((1f - size) * Vector3.Distance(this.queue.toObject.position, this.toObjectStart)));
                        }

                        if(size > 0f) {
                            this.transform.localScale = Vector3.one * size;
                        } else {
                            this.gameObject.SetActive(false);
                        }
                        break;
                }
                
                this.lifeTime += Time.deltaTime;
            } else {
                this.toObjectStart = this.transform.position;
            }
            this.delay -= Time.deltaTime;
        }
    }
}