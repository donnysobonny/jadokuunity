using System;
using UnityEngine;
using UnityEngine.UI;

namespace Jadoku.UI {
    public abstract class DebugGUIBase : MonoBehaviour {

        public bool scaleWithSceenSize = true;

        public float designWidth = 360f;
        public float designHeight = 640f;

        public float scale = 1f;

        public abstract void DrawGUI();
        
        protected virtual void OnGUIOpen() {}
        protected virtual void OnGUIClose() {}
        
        protected void Header(string text) {
            this.BigSpace();
            GUILayout.Label("<b><size=18>" + text + "</size></b>");
            this.NormalSpace();
        }

        protected void Space(float pixels) {
            GUILayout.Space(pixels);
        }
        public void BigSpace() {
            this.Space(7.5f);
        }
        public void SmallSpace() {
            this.Space(2.5f);
        }
        public void NormalSpace() {
            this.Space(5f);
        }

        protected float FloatSlider(float value, float min, float max, string name, string description, int decimalPoints = 2, Action<float> onChange = null) {
            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            GUILayout.Label("<b>" + name + "</b>", GUILayout.ExpandWidth(false));
            float result = GUILayout.HorizontalSlider(value, min, max, GUILayout.ExpandWidth(true));
            if(onChange != null && value != result) {
                onChange(result);
            }
            GUILayout.Label(string.Format("{0:n" + decimalPoints + "}", value), GUILayout.ExpandWidth(false));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            GUILayout.Label("<size=12>+ </size>", GUILayout.ExpandWidth(false));
            GUILayout.Label("<size=12>" + description + "</size>");
            GUILayout.EndHorizontal();

            this.NormalSpace();
            return result;
        }

        public int IntSlider(int value, int min, int max, string name, string description, Action<int> onChange = null) {
            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            GUILayout.Label("<b>" + name + "</b>", GUILayout.ExpandWidth(false));
            int result = Mathf.RoundToInt(GUILayout.HorizontalSlider(value, min, max, GUILayout.ExpandWidth(true)));
            if(onChange != null && value != result) {
                onChange(result);
            }
            GUILayout.Label(string.Format("{0:n0}", value), GUILayout.ExpandWidth(false));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            GUILayout.Label("<size=12>+ </size>", GUILayout.ExpandWidth(false));
            GUILayout.Label("<size=12>" + description + "</size>");
            GUILayout.EndHorizontal();
            
            this.NormalSpace();
            return result;
        }

        public bool Toggle(bool value, string name, string description, Action<bool> onChange = null) {
            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            bool result = GUILayout.Toggle(value, "<b>" + name + "</b>");
            if(onChange != null && value != result) {
                onChange(result);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            GUILayout.Label("<size=12>+ </size>", GUILayout.ExpandWidth(false));
            GUILayout.Label("<size=12>" + description + "</size>");
            GUILayout.EndHorizontal();

            this.NormalSpace();
            return result;
        }

        public string Textfield(string value, string name, string description, Action<string> onChange = null) {
            this.Space(2);
            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            GUILayout.Label("<b>" + name + "</b>", GUILayout.ExpandWidth(false));
            string result = GUILayout.TextField(value, GUILayout.ExpandWidth(true));
            if(onChange != null && value != result) {
                onChange(result);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            GUILayout.Label("<size=12>+ </size>", GUILayout.ExpandWidth(false));
            GUILayout.Label("<size=12>" + description + "</size>");
            GUILayout.EndHorizontal();

            this.NormalSpace();
            return result;
        }

        bool on = false;
        Vector2 scrollPosition = Vector2.zero;
        public void OnGUI() {
            GUI.skin.label.margin = new RectOffset(5, 5, 0, 0);
            GUI.skin.label.padding = new RectOffset(0, 0, 0, 0);
            GUI.skin.horizontalSlider.margin = new RectOffset(5, 5, -1, 0);
            GUI.skin.horizontalSlider.fixedHeight = 20f;
            GUI.skin.horizontalSliderThumb.fixedHeight = GUI.skin.horizontalSliderThumb.fixedWidth = 20f;
            GUI.skin.toggle.margin = new RectOffset(5, 5, 0, 2);
            GUI.skin.toggle.padding = new RectOffset(20, 0, 2, 0);
            GUI.skin.textField.margin = new RectOffset(5, 5, 0, 0);
            GUI.skin.textField.padding = new RectOffset(3, 3, 1, 1);

            if(this.scaleWithSceenSize) {
                GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3((float)(Screen.width) / this.designWidth, (float)(Screen.height) / this.designHeight, 1));
            } else {
                GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(1, 1, 1));
            }            

            if(this.on) {
                GUI.Box(new Rect(5, 5, Screen.width - 10f, Screen.height - 40f), GUIContent.none);
                GUI.Box(new Rect(5, 5, Screen.width - 10f, Screen.height - 40f), GUIContent.none);
                GUILayout.BeginArea(new Rect(5, 5, Screen.width - 10f, Screen.height - 40f));

                this.scrollPosition = GUILayout.BeginScrollView(this.scrollPosition, GUILayout.ExpandWidth(true));

                this.DrawGUI();

                GUILayout.EndScrollView();

                GUILayout.EndArea();

                if(GUI.Button(new Rect(Screen.width - 200f, Screen.height - 25f, 200f, 25f), "CLOSE DEBUG MENU")) {
                    this.ToggleGUI();
                }
            } else {
                if(GUI.Button(new Rect(Screen.width - 200f, Screen.height - 25f, 200f, 25f), "OPEN DEBUG MENU")) {
                    this.ToggleGUI();
                }
            }
        }

        private void ToggleGUI() {
            this.on = !this.on;
            foreach(GraphicRaycaster grcs in FindObjectsOfType<GraphicRaycaster>()) {
                grcs.enabled = !this.on;
            }
            if (this.on) {
                this.OnGUIOpen();   
            } else {
                this.OnGUIClose();
            }
        }
    }
}