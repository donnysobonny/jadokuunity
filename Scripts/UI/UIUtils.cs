﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Jadoku.UI {
    public static class UIUtils {
        public static WebPosition RectTransformToWebPostiion(RectTransform rectTransform) {
            Vector3[] vects = new Vector3[4];
            rectTransform.GetWorldCorners(vects);

            WebPosition wp = new WebPosition();
            wp.left = vects[0].x;
            wp.bottom = vects[0].y;
            wp.right = Screen.width - vects[2].x;
            wp.top = Screen.height - vects[2].y;
            wp.width = Screen.width - wp.left - wp.right;
            wp.height = Screen.height - wp.top - wp.bottom;

            return wp;
        }
    }
}