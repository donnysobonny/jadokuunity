﻿using UnityEngine;
using UnityEngine.UI;

namespace Jadoku.UI {
    public class FloatingMessage : MonoBehaviour {
        public Text text1;
        public Text text2;
        public Shadow shadow1;
        public Shadow shadow2;
        public float speed;
        public float lifeTime;

        public FloatingMessageQueueDirection direction;

        private float startTime = 0f;
        public float startAlpha = 1f;

        void Start() {
            this.startTime = Time.realtimeSinceStartup;
        }

        void OnEnable() {
            this.startTime = Time.realtimeSinceStartup;
        }

        void Update() {
            this.transform.position += this.direction == FloatingMessageQueueDirection.up ? new Vector3(0f, 1f, 0f) * Time.deltaTime * this.speed : new Vector3(0f, -1f, 0f) * Time.deltaTime * this.speed;

            float alpha = 1f - ((Time.realtimeSinceStartup - this.startTime) / this.lifeTime);
            alpha *= this.startAlpha;
            if(alpha > 0f) {
                this.text1.color = this.text2.color = new Color(
                    this.text1.color.r,
                    this.text1.color.g,
                    this.text1.color.b,
                    alpha    
                );
                this.shadow1.effectColor = this.shadow2.effectColor = new Color(
                    this.shadow1.effectColor.r,
                    this.shadow1.effectColor.g,
                    this.shadow1.effectColor.b,
                    alpha    
                );
            } else {
                this.gameObject.SetActive(false);
            }
        }
    }
}