﻿using Jadoku.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace Jadoku.UI {
    public class FloatingMessageQueue : MonoBehaviour {
        public FloatingMessageQueueDirection direction = FloatingMessageQueueDirection.up;
        public float speed = 10f;
        public float lifeTime = 2f;
        public float messageInverval = 0.2f;

        public Font font;
        public int fontSize;

        public int initialPoolSize = 10;

        private ObjectPool<FloatingMessage> floatingMessagePool;

        private List<QueuedMessage> messages = new List<QueuedMessage>();

        public void AddMessage(string message1, string message2, Color fontColor) {
            this.AddMessage(message1, message2, fontColor, false, new Color(), new Vector2());
        }
        public void AddMessage(string message1, string message2, Color fontColor, Color shadowColor, Vector2 shadowDistance) {
            this.AddMessage(message1, message2, fontColor, true, shadowColor, shadowDistance);
        }

        private void AddMessage(string message1, string message2, Color fontColor, bool shadow, Color shadowColor, Vector2 shadowDistance) {
            this.messages.Add(new QueuedMessage(message1, message2, fontColor, shadow, shadowColor, shadowDistance));
        }

        // Use this for initialization
        void Start() {
            this.floatingMessagePool = new ObjectPool<FloatingMessage>(Resources.Load<FloatingMessage>("UI/FloatingMessage"), this.initialPoolSize);
        }

        private float nextMessage = 0f;
        void Update() {
            if(this.messages.Count > 0) {
                if(Time.realtimeSinceStartup > this.nextMessage) {
                    this.nextMessage = Time.realtimeSinceStartup + this.messageInverval;

                    QueuedMessage qm = this.messages[0];
                    this.messages.RemoveAt(0);

                    FloatingMessage fm = this.floatingMessagePool.GetObject();
                    fm.transform.SetParent(this.transform);
                    fm.transform.localScale = Vector3.one;
                    fm.transform.localPosition = Vector3.zero;
                    fm.transform.localEulerAngles = Vector3.zero;
                    fm.speed = this.speed;
                    fm.lifeTime = this.lifeTime;
                    fm.direction = this.direction;

                    fm.text1.text = qm.message1;
                    fm.text2.text = qm.message2;
                    fm.text1.color = fm.text2.color = qm.fontColor;
                    fm.startAlpha = qm.fontColor.a;
                    fm.text1.font = fm.text2.font = this.font;
                    fm.text1.fontSize = fm.text2.fontSize = this.fontSize;
                    fm.shadow1.enabled = fm.shadow2.enabled = qm.shadow;
                    if (qm.shadow) {
                        fm.shadow1.effectColor = fm.shadow2.effectColor = qm.shadowColor;
                        fm.shadow1.effectDistance = fm.shadow2.effectDistance = qm.shadowDistance;
                    }
                }
            }
        }
    }

    public enum FloatingMessageQueueDirection {
        up,
        down
    }

    public class QueuedMessage {
        public Color fontColor;
        public string message1;
        public string message2;
        public bool shadow;
        public Color shadowColor;
        public Vector2 shadowDistance;

        public QueuedMessage(string message1, string message2, Color color, bool shadow, Color shadowColor, Vector2 shadowDistance) {
            this.message1 = message1;
            this.message2 = message2;
            this.fontColor = color;
            this.shadow = shadow;
            this.shadowColor = shadowColor;
            this.shadowDistance = shadowDistance;
        }
    }
}