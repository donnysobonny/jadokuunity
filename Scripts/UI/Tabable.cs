﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Jadoku.UI {
    public class Tabable : MonoBehaviour {

        private EventSystem system;

        // Use this for initialization
        void Start() {
            this.system = EventSystem.current;
        }

        // Update is called once per frame
        void Update() {
            if(Input.GetKeyDown(KeyCode.Tab)) {
                Selectable next = null;
                if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
                    next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnUp();
                    if(!next) {
                        next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnLeft();
                    }
                } else {
                    next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
                    if(!next) {
                        next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnRight();
                    }
                }

                if(next != null) {
                    InputField inputfield = next.GetComponent<InputField>();
                    if(inputfield != null) inputfield.OnPointerClick(new PointerEventData(system));  //if it's an input field, also set the text caret

                    system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
                }
            }
        }
    }
}