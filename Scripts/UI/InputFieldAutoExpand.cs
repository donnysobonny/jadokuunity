﻿using UnityEngine;
using UnityEngine.UI;

namespace Jadoku.UI {
    [RequireComponent(typeof(InputField), typeof(LayoutElement))]
    public class InputFieldAutoExpand : MonoBehaviour {
        public InputField input;
        public LayoutElement layoutElement;

        public bool controlHeight = false;
        public bool controlWidth = false;
        public float minWidth = 30f;
        public float minHeight = 20f;
        public float horizontalPadding = 0f;
        public float verticalPadding = 0f;

        private void Start() {
            this.input.onValueChanged.RemoveListener(this.OnInputChange);
            this.input.onValueChanged.AddListener(this.OnInputChange);

            if(this.controlWidth) {
                this.layoutElement.preferredWidth = this.layoutElement.minWidth = this.minWidth;
            }
            if(this.controlHeight) {
                this.layoutElement.preferredHeight = this.layoutElement.minHeight = this.minHeight;
            }
        }

        public void OnInputChange(string text) {
            if(this.controlHeight || this.controlWidth) {
                TextGenerationSettings settings = this.input.textComponent.GetGenerationSettings(this.input.textComponent.rectTransform.rect.size);
                settings.generateOutOfBounds = false;
                if(this.controlHeight) {
                    this.layoutElement.preferredHeight = this.layoutElement.minHeight = Mathf.Max(this.minHeight, new TextGenerator().GetPreferredHeight(this.input.text, settings) + this.verticalPadding);
                }
                if(this.controlWidth) {
                    this.layoutElement.preferredWidth = this.layoutElement.minWidth = Mathf.Max(this.minWidth, new TextGenerator().GetPreferredWidth(this.input.text, settings) + this.horizontalPadding);
                }
            }
        }
    }
}
