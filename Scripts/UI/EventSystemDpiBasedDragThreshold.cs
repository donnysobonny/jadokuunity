﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Jadoku.UI {
    public class EventSystemDpiBasedDragThreshold : MonoBehaviour {
        private void Start() {
            int defaultValue = EventSystem.current.pixelDragThreshold;
            EventSystem.current.pixelDragThreshold = Mathf.Max(defaultValue, (int)(defaultValue * Screen.dpi / 160f));
        }
    }
}
