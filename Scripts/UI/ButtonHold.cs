﻿using Jadoku.Utils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Jadoku.UI {
    [RequireComponent(typeof(Button))]
    [ExecuteInEditMode]
    public class ButtonHold : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

        Button button;
        
        [Tooltip("the event triggered when a click is handled, either from the initial click, or subsequent clicks")]
        public UnityEvent processClick;
        [Tooltip("how long after the initial click to start the hold clicks")]
        public float initialDelay = 0.5f;
        [Tooltip("the min/max delay between clicks while holding. It starts at maximum and reaches minimum at the end of the hold lifetime")]
        public MinMax clickInterval;
        [Tooltip("how long it should take to reach the minimum interval")]
        public float lifetime = 5f;

        private void OnEnable() {
            if(this.button == null) {
                this.button = GetComponent<Button>();
            }
        }

        bool down = false;
        float currentLifetime = 0f;
        float nextClick = 0f;

        public void OnPointerDown(PointerEventData eventData) {
            if(this.button.interactable && !eventData.dragging) {
                this.down = true;
                this.currentLifetime = 0f;
                this.nextClick = Time.time + this.initialDelay;
            }
        }

        public void OnPointerUp(PointerEventData eventData) {
            this.down = false;
            //if up before hold, interact
            if(this.button.interactable && !eventData.dragging && Time.time <= this.nextClick) {
                this.processClick.Invoke();
            }
        }

        private void Update() {
            if(this.down) {
                if(this.button.interactable) {
                    this.currentLifetime += Time.deltaTime;
                    if(Time.time > this.nextClick) {
                        this.nextClick = Time.time + this.clickInterval.GetValue(1f - (this.currentLifetime / this.lifetime));
                        this.processClick.Invoke();
                    }
                }
            }
        }
    }
}
