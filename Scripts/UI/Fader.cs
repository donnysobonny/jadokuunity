﻿using System;
using UnityEngine;

namespace Jadoku.UI {
    public class Fader : MonoBehaviour {
        const float alphaCutoff = 0.1f;

        public float fadeSpeed = 2f;
        public float maxAlpha = 1f;
        public CanvasGroup canvasGroup;

        public bool disableOnFadedOut = true;

        private float targetAlpha = 0f;

        Action onFadeIn;
        Action onFadeOut;

        private void Awake() {
            if(this.canvasGroup == null) {
                Debug.LogError("fader exists without an assigned canvas group!", this.gameObject);
            }

            this.targetAlpha = this.canvasGroup.alpha;
            if(this.targetAlpha > this.maxAlpha) {
                this.targetAlpha = this.maxAlpha;
            }
            if(this.disableOnFadedOut && this.canvasGroup.alpha <= 0f) {
                this.gameObject.SetActive(false);
            }
        }

        public void FadeIn() {
            this.FadeIn(null);
        }
        public void FadeIn(Action onComplete) {
            if(this.disableOnFadedOut) {
                this.gameObject.SetActive(true);
            }
            this.targetAlpha = this.maxAlpha;
            this.onFadeIn = onComplete;
            this.fadingOut = false;
        }

        bool fadingOut = false;
        public void FadeOut() {
            this.FadeOut(null);
        }
        public void FadeOut(Action onComplete) {
            this.fadingOut = true;
            this.targetAlpha = 0f;
            this.onFadeOut = onComplete;
        }

        public void SetAlpha(float alpha) {
            alpha = Mathf.Clamp(alpha, 0f, this.maxAlpha);
            if(this.disableOnFadedOut && alpha > 0f) {
                this.gameObject.SetActive(true);
            }
            this.canvasGroup.alpha = this.targetAlpha = alpha;
            this.onFadeIn = this.onFadeOut = null;
            this.fadingOut = false;
        }

        public bool IsNotFadedIn() {
            return this.canvasGroup.alpha < (this.maxAlpha - alphaCutoff);
        }
        public bool IsNotFadedOut() {
            return this.canvasGroup.alpha > alphaCutoff;
        }
        public bool IsFadedIn() {
            return this.canvasGroup.alpha > (this.maxAlpha - alphaCutoff);
        }
        public bool IsFadedOut() {
            return this.canvasGroup.alpha < alphaCutoff;
        }

        void Update() {
            if(this.canvasGroup == null) {
                return;
            }

            if(this.canvasGroup.alpha != this.targetAlpha) {
                this.canvasGroup.alpha = Mathf.MoveTowards(this.canvasGroup.alpha, this.targetAlpha, Time.deltaTime * this.fadeSpeed);
            }

            if(this.onFadeIn != null) {
                if(this.IsFadedIn()) {
                    this.onFadeIn();
                    this.onFadeIn = null;
                }
            }

            if(this.onFadeOut != null) {
                if(this.IsFadedOut()) {
                    this.onFadeOut();
                    this.onFadeOut = null;
                }
            }

            this.canvasGroup.blocksRaycasts = this.IsNotFadedOut();
            this.canvasGroup.interactable = this.IsFadedIn();

            if(this.disableOnFadedOut && this.fadingOut && this.IsFadedOut()) {
                this.gameObject.SetActive(false);
            }
        }
    }
}
