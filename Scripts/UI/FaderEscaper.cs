﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Jadoku.UI {
    public class FaderEscaper : MonoBehaviour {
        public bool edit;

        public List<Fader> faders;
        public UnityEvent onDefault;

        void Update() {
            if(Input.GetKeyDown(KeyCode.Escape)) {
                foreach(Fader fader in this.faders) {
                    if(fader.IsFadedIn()) {
                        fader.FadeOut();
                        return;
                    }
                }
                //no fader faded out
                this.onDefault.Invoke();
            }
        }
    }
}
