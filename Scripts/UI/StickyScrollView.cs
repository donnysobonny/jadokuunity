﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Jadoku.UI {
    /// <summary>
    /// This is used to segment the scrollable area of a scroll view, making the scroll lerp to the nearest segment
    /// </summary>
    [RequireComponent(typeof(ScrollRect))]
    [RequireComponent(typeof(EventTrigger))]
    public class StickyScrollView : MonoBehaviour {

        public EventTrigger eventTrigger;
        public ScrollRect scrollRect;

        public int horizontalSegments = 1;
        public int verticalSegments = 1;

        public float lerpSpeed = 1f;

        public bool dragging = false;

        void Start() {
            if(this.scrollRect == null) {
                this.scrollRect = GetComponent<ScrollRect>();
            }
            if(this.eventTrigger == null) {
                this.eventTrigger = GetComponent<EventTrigger>();
            }
            if(this.eventTrigger.triggers.Count != 2) {
                EventTrigger.Entry beginDrag = new EventTrigger.Entry();
                beginDrag.eventID = EventTriggerType.BeginDrag;
                beginDrag.callback.AddListener(delegate (BaseEventData e) {
                    this.dragging = true;
                });
                this.eventTrigger.triggers.Add(beginDrag);

                EventTrigger.Entry endDrag = new EventTrigger.Entry();
                endDrag.eventID = EventTriggerType.EndDrag;
                endDrag.callback.AddListener(delegate (BaseEventData e) {
                    this.dragging = false;
                });
                this.eventTrigger.triggers.Add(endDrag);
            }
        }

        // Update is called once per frame
        void Update() {
            this.Lerp();
        }
        
        float horizontal = 0f;
        float vertical = 0f;

        public float moveThreshold = 1f;

        bool overrideHorizontal = false;
        public void OverrideHorizontal(float horizontal) {
            this.overrideHorizontal = true;
            this.horizontal = horizontal;
        }
        bool overrideVertical = false;
        public void OverrideVertical(float vertical) {
            this.overrideVertical = true;
            this.vertical = vertical;
        }
        
        Vector2 oldMousePos;
        void Lerp() {
            if(this.overrideHorizontal) {
                this.scrollRect.horizontalNormalizedPosition = Mathf.Lerp(this.scrollRect.horizontalNormalizedPosition, this.horizontal, Time.deltaTime * this.lerpSpeed);

                if(Mathf.Abs(this.scrollRect.horizontalNormalizedPosition - this.horizontal) <= 0.1f) {
                    this.overrideHorizontal = false;
                }
            }
            if(this.overrideVertical) {
                this.scrollRect.verticalNormalizedPosition = Mathf.Lerp(this.scrollRect.verticalNormalizedPosition, this.vertical, Time.deltaTime * this.lerpSpeed);

                if(Mathf.Abs(this.scrollRect.verticalNormalizedPosition - this.vertical) <= 0.1f) {
                    this.overrideVertical = false;
                }
            }
            if(this.overrideHorizontal || this.overrideVertical) {
                return;
            }

            float lowestDist = Mathf.Infinity;
            float segmentSize = 1f / (this.horizontalSegments - 1);
            if(this.horizontalSegments > 1) {
                if(this.dragging) {
                    //find the next segment, based on the direction of mouse movement
                    if(
                        (Mathf.Abs(Input.mousePosition.x - this.oldMousePos.x) / Time.deltaTime >= this.moveThreshold) &&
                        Input.mousePosition.x < this.oldMousePos.x
                    ) {
                        //going right
                        for(int i = 0; i < this.horizontalSegments; i++) {
                            if((i * segmentSize) >= this.scrollRect.horizontalNormalizedPosition) {
                                float dist = Mathf.Abs((i * segmentSize) - this.scrollRect.horizontalNormalizedPosition);
                                if(dist < lowestDist) {
                                    lowestDist = dist;
                                    this.horizontal = i * segmentSize;
                                }
                            }
                        }
                    } else if(
                        (Mathf.Abs(Input.mousePosition.x - this.oldMousePos.x) / Time.deltaTime >= this.moveThreshold) &&
                        Input.mousePosition.x > this.oldMousePos.x
                    ) {
                        //going left
                        for(int i = 0; i < this.horizontalSegments; i++) {
                            if((i * segmentSize) <= this.scrollRect.horizontalNormalizedPosition) {
                                float dist = Mathf.Abs((i * segmentSize) - this.scrollRect.horizontalNormalizedPosition);
                                if(dist < lowestDist) {
                                    lowestDist = dist;
                                    this.horizontal = i * segmentSize;
                                }
                            }
                        }
                    }
                } else {
                    //otherwise, lerp to segment
                    this.scrollRect.horizontalNormalizedPosition = Mathf.Lerp(this.scrollRect.horizontalNormalizedPosition, this.horizontal, Time.deltaTime * this.lerpSpeed);
                }                
            }
            
            lowestDist = Mathf.Infinity;
            segmentSize = 1f / (this.verticalSegments - 1);
            if(this.verticalSegments > 1) {
                if(this.dragging) {
                    if(
                        (Mathf.Abs(Input.mousePosition.y - this.oldMousePos.y) / Time.deltaTime >= this.moveThreshold) &&
                        Input.mousePosition.y > this.oldMousePos.y
                    ) {
                        //going up
                        for(int i = 0; i < this.verticalSegments; i++) {
                            if((i * segmentSize) >= this.scrollRect.verticalNormalizedPosition) {
                                float dist = Mathf.Abs((i * segmentSize) - this.scrollRect.verticalNormalizedPosition);
                                if(dist < lowestDist) {
                                    lowestDist = dist;
                                    this.vertical = i * segmentSize;
                                }
                            }
                        }
                    } else if(
                        (Mathf.Abs(Input.mousePosition.y - this.oldMousePos.y) / Time.deltaTime >= this.moveThreshold) &&
                        Input.mousePosition.y < this.oldMousePos.y
                    ) {
                        //going down
                        for(int i = 0; i < this.verticalSegments; i++) {
                            if((i * segmentSize) <= this.scrollRect.verticalNormalizedPosition) {
                                float dist = Mathf.Abs((i * segmentSize) - this.scrollRect.verticalNormalizedPosition);
                                if(dist < lowestDist) {
                                    lowestDist = dist;
                                    this.vertical = i * segmentSize;
                                }
                            }
                        }
                    }
                } else {
                    this.scrollRect.verticalNormalizedPosition = Mathf.Lerp(this.scrollRect.verticalNormalizedPosition, this.vertical, Time.deltaTime * this.lerpSpeed);
                }                
            }

            this.oldMousePos = Input.mousePosition;
        }
    }
}
