﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Jadoku.UI {
    public class OnSubmit : MonoBehaviour {

        public UnityEvent onSubmit;

        private EventSystem eventSystem;

        // Use this for initialization
        void Start() {
            this.eventSystem = EventSystem.current;
        }

        // Update is called once per frame
        void Update() {
            if((Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.KeypadEnter)) && this.eventSystem.currentSelectedGameObject == this.gameObject) {
                if(this.onSubmit != null) {
                    this.onSubmit.Invoke();
                }
            }
        }
    }
}