﻿using UnityEngine;
using UnityEngine.UI;

namespace Jadoku.UI {
    [RequireComponent(typeof(Text))]
    [ExecuteInEditMode]
    public class TextLimiter : MonoBehaviour {

        public Text textComponent;

        public bool basedOnWidth = false;
        public RectTransform widthAnchor;
        public float widthToCaracterLimit = 1f;
        public int characterLimit = 10;
        public string appendIfLimited = "...";

        string processed = "";

        private void Update() {
            if(this.textComponent == null) {
                this.textComponent = GetComponent<Text>();
            }
            
            if(this.textComponent.text != this.processed) {
                if(this.basedOnWidth && this.widthAnchor != null) {
                    this.characterLimit = Mathf.RoundToInt(this.widthAnchor.rect.width * this.widthToCaracterLimit * this.textComponent.fontSize);
                }

                if(this.textComponent.text.Length - this.appendIfLimited.Length > this.characterLimit - this.appendIfLimited.Length) {
                    this.processed = this.textComponent.text.Substring(0, characterLimit - this.appendIfLimited.Length) + this.appendIfLimited;
                } else {
                    this.processed = this.textComponent.text;
                }
                this.textComponent.text = this.processed;
            }
        }
    }
}
