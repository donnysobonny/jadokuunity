﻿using UnityEngine;

namespace Jadoku.UI {
    [ExecuteInEditMode]
    public class WorldCanvasScaleWithCamera : MonoBehaviour {

        public Canvas canvas;
        public float scaleModifier = 1f;

        // Update is called once per frame
        void Update() {
            this.canvas.transform.localScale = Vector3.one * this.canvas.worldCamera.orthographicSize * this.scaleModifier;
        }
    }
}