﻿namespace Jadoku.UI {
    public class WebPosition {
        public float left;
        public float right;
        public float bottom;
        public float top;
        public float width;
        public float height;

        public WebPosition() { }

        public WebPosition(float left, float right, float top, float bottom, float width, float height) {
            this.left = left;
            this.right = right;
            this.top = top;
            this.bottom = bottom;
            this.width = width;
            this.height = height;
        }
    }
}