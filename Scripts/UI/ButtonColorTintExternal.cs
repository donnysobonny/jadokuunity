﻿using UnityEngine;
using UnityEngine.UI;

namespace Jadoku.UI {
    [ExecuteInEditMode]
    [RequireComponent(typeof(Graphic))]
    public class ButtonColorTintExternal : MonoBehaviour {
        public Button button;
        public Graphic graphic;
        public bool inheritButtonColors = false;
        public Color normalColor = Color.white;
        public Color disabledColor = Color.white;
        private void OnEnable() {
            if(this.graphic == null) {
                this.graphic = this.GetComponent<Graphic>();
            }
        }

        public void Start() {
            if(this.button == null) {
                Debug.LogWarning("Button is not assigned in inspector", this.gameObject);
            }
            if(this.button != null && this.graphic != null) {
                this.CommitUpdate();
                this.oldIsInteractive = this.button.IsInteractable();
            }
        }

        bool oldIsInteractive = false;
        private void Update() {
            if(this.button != null && this.graphic != null) {
                if(this.oldIsInteractive != this.button.IsInteractable()) {
                    this.CommitUpdate();
                }
                this.oldIsInteractive = this.button.IsInteractable();
            }
        }

        void CommitUpdate() {
            if(this.button.IsInteractable()) {
                if(this.inheritButtonColors) {
                    this.graphic.color = this.button.colors.normalColor;
                } else {
                    this.graphic.color = this.normalColor;
                }
            } else {
                if(this.inheritButtonColors) {
                    this.graphic.color = this.button.colors.disabledColor;
                } else {
                    this.graphic.color = this.disabledColor;
                }
            }
        }
    }
}
