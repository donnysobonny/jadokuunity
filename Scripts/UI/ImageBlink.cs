﻿using UnityEngine;
using UnityEngine.UI;

namespace Jadoku.UI {
    [RequireComponent(typeof(Image))]
    public class ImageBlink : MonoBehaviour {
        public Image image;
        public float blinkInterval;
        public bool blinkOnStart = false;

        private void Start() {
            if(this.blinkOnStart) {
                this.StartBlink();
            }
        }

        private void OnEnable() {
            if(this.blinkOnStart) {
                this.StartBlink();
            }
        }

        bool blinking = false;
        float nextBlink = 0f;
        public void StartBlink() {
            this.blinking = true;
            this.image.enabled = true;
            this.nextBlink = Time.time + this.blinkInterval;
        }
        public void StopBlink() {
            this.image.enabled = true;
            this.blinking = false;
        }

        private void Update() {
            if(this.blinking && Time.time > this.nextBlink) {
                this.nextBlink = Time.time + this.blinkInterval;
                this.image.enabled = !this.image.enabled;
            }
        }
    }
}
