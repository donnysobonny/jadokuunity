﻿using UnityEngine;
using UnityEngine.UI;

namespace RS.UI {
    /// <summary>
    /// A utility for sizing the cells of a grid layout group to the width/height of a Rect Transform
    /// </summary>
    public class GridLayoutGroupSizer : MonoBehaviour {
        public GridLayoutGroup gridLayoutGroup;
        public RectTransform anchor;

        public bool controlHeight;
        public bool controlWidth;

        void Update() {
            if(this.controlWidth || this.controlHeight) {
                Vector2 v = this.gridLayoutGroup.cellSize;
                if(this.controlHeight) {
                    v.y = this.anchor.rect.height;
                }
                if(this.controlWidth) {
                    v.x = this.anchor.rect.width;
                }
                this.gridLayoutGroup.cellSize = v;
            }
        }
    }
}
