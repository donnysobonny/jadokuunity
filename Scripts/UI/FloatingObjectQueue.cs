﻿using Jadoku.Utils;
using UnityEngine;

namespace Jadoku.UI {
    public class FloatingObjectQueue : MonoBehaviour {
        public FloatingObjectQueueDirection direction = FloatingObjectQueueDirection.up;
        public FloatingObjectQueueFade fade = FloatingObjectQueueFade.fade;
        public float speed = 10f;
        public float horizontalBounceSpeed = 0.1f;
        public float lifeTime = 2f;
        public float lifeTimeDelay = 0f;
        public int initialPoolSize = 10;
        public FloatingObject prefab;
        public bool useLocalSpace;
        public Transform toObject;
        public bool floatToObject = false;

        private ObjectPool<FloatingObject> pool;

        private void Awake() {
            this.pool = new ObjectPool<FloatingObject>(this.prefab, this.initialPoolSize, this.transform);
            switch(this.direction) {
                case FloatingObjectQueueDirection.pingPongBounceUp:
                    this.nextPingPongDirection = FloatingObjectQueueDirection.bounceUpRight;
                    break;
                case FloatingObjectQueueDirection.pingPongBounceDown:
                    this.nextPingPongDirection = FloatingObjectQueueDirection.bounceDownRight;
                    break;
            }
        }

        FloatingObjectQueueDirection nextPingPongDirection = FloatingObjectQueueDirection.bounceUpRight;
        public T AddObject<T>(Vector3 position, Quaternion rotation, float alpha) where T : FloatingObject {
            FloatingObjectQueueDirection direction = this.direction;
            switch(direction) {
                case FloatingObjectQueueDirection.randomBounceUp:
                    direction = Random.Range(0f, 1f) > 0.5f ? FloatingObjectQueueDirection.bounceUpRight : FloatingObjectQueueDirection.bounceUpLeft;
                    break;
                case FloatingObjectQueueDirection.randomBounceDown:
                    direction = Random.Range(0f, 1f) > 0.5f ? FloatingObjectQueueDirection.bounceDownRight : FloatingObjectQueueDirection.bounceDownLeft;
                    break;
                case FloatingObjectQueueDirection.pingPongBounceUp:
                    direction = this.nextPingPongDirection;
                    this.nextPingPongDirection = this.nextPingPongDirection == FloatingObjectQueueDirection.bounceUpRight ? FloatingObjectQueueDirection.bounceUpLeft : FloatingObjectQueueDirection.bounceUpRight;
                    break;
                case FloatingObjectQueueDirection.pingPongBounceDown:
                    direction = this.nextPingPongDirection;
                    this.nextPingPongDirection = this.nextPingPongDirection == FloatingObjectQueueDirection.bounceDownLeft ? FloatingObjectQueueDirection.bounceDownRight : FloatingObjectQueueDirection.bounceDownLeft;
                    break;
            }

            FloatingObject fo = this.pool.GetObjectDisabled();
            fo.Init(this, direction, position, rotation, alpha, this.useLocalSpace);
            return fo as T;
        }

        public T AddObject<T>(Vector3 position, float alpha) where T : FloatingObject {
            return this.AddObject<T>(position, Quaternion.identity, alpha);
        }

        public T AddObject<T>(float alpha) where T : FloatingObject {
            return this.AddObject<T>(Vector3.zero, Quaternion.identity, alpha);
        }

        public T AddObject<T>() where T : FloatingObject {
            return this.AddObject<T>(Vector3.zero, Quaternion.identity, 1f);
        }

        public FloatingObject AddObject(Vector3 position, Quaternion rotation, float alpha) {
            return this.AddObject<FloatingObject>(position, rotation, alpha);
        }

        public FloatingObject AddObject(Vector3 position, float alpha) {
            return this.AddObject<FloatingObject>(position, alpha);
        }

        public FloatingObject AddObject(float alpha) {
            return this.AddObject<FloatingObject>(alpha);
        }

        public FloatingObject AddObject() {
            return this.AddObject<FloatingObject>();
        }
    }

    public enum FloatingObjectQueueDirection {
        up,
        down,
        left,
        right,
        upRight,
        upLeft,
        downRight,
        downLeft,
        bounceUpRight,
        bounceUpLeft,
        bounceDownRight,
        bounceDownLeft,
        randomBounceUp,
        randomBounceDown,
        pingPongBounceUp,
        pingPongBounceDown,
    }

    public enum FloatingObjectQueueFade {
        fade,
        shrink
    }
}