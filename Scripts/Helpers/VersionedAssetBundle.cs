﻿using UnityEngine;

namespace Jadoku.Helpers {
    [System.Serializable]
    public class VersionedAssetBundle {
        public string name;
        public int version;
    }
}