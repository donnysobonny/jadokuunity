﻿using System;
using UnityEngine;

namespace Jadoku.DynamicImages {
    public abstract class IDynamicImageLoader : MonoBehaviour {
        public bool loadOnStart = false;
        public bool loadOnEnable = false;
        public string imageUrl;
        public bool cacheInMemory = true;

        public abstract void LoadImage(Action onLoad = null);

        protected void Error(string error) {
            Debug.LogErrorFormat("DYNAMICIMAGELOADER failed to load image {0}: {2}", this.imageUrl, error);
        }
    }
}
