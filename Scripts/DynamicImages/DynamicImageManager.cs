﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jadoku.DynamicImages {
    public class DynamicImageManager : MonoBehaviour {

        public static DynamicImageManager instance { get; private set; }

        static Dictionary<string, Texture2D> cachedTextures = new Dictionary<string, Texture2D>();
        static Dictionary<string, Sprite> cachedSprites = new Dictionary<string, Sprite>();
        static List<string> loadingTextures = new List<string>();
        static List<string> loadingSprites = new List<string>();

        void Awake() {
            instance = this;
        }

        public static void LoadTexture2D(string url, Action<Texture2D> onLoad, Action<string> onError = null, bool cacheInMemory = true) {
            if(cachedTextures.ContainsKey(url)) {
                onLoad(cachedTextures[url]);
            } else {
                instance.StartCoroutine(instance.DownloadTexture(new WWW(url), onLoad, onError, cacheInMemory));
            }
        }

        public static void LoadSprite(string url, Action<Sprite> onLoad, Action<string> onError = null, bool cacheInMemory = true) {
            if(cachedSprites.ContainsKey(url)) {
                onLoad(cachedSprites[url]);
            } else {
                instance.StartCoroutine(instance.DownloadSprite(new WWW(url), onLoad, onError, cacheInMemory));
            }
        }

        IEnumerator DownloadSprite(WWW req, Action<Sprite> onLoad, Action<string> onError = null, bool cacheInMemory = true) {
            while(!req.isDone) {
                yield return null;
            }
            while(loadingSprites.Contains(req.url)) {
                yield return null;
            }

            if(cachedSprites.ContainsKey(req.url)) {
                onLoad(cachedSprites[req.url]);
            } else {
                loadingSprites.Add(req.url);

                if(req.error != null && req.error.Length > 0) {
                    if(onError != null) {
                        onError(req.error);
                    }
                    Debug.LogErrorFormat("DYNAMICIMAGEMANAGER could not load image {0}: {1}", req.url, req.error);
                } else {
                    Texture2D image = req.texture;
                    if(image != null) {
                        Sprite s = Sprite.Create(image, new Rect(0f, 0f, image.width, image.height), new Vector2(0.5f, 0.5f), 100f);
                        if(cacheInMemory) {
                            cachedSprites[req.url] = s;
                        }
                        onLoad(s);
                        s = null;
                    } else {
                        if(onError != null) {
                            onError("Could not generate an image from the url " + req.url);
                        }
                        Debug.LogErrorFormat("DYNAMICIMAGEMANAGER could not generate an image from the url {0}", req.url);
                    }
                    image = null;
                }

                loadingSprites.Remove(req.url);
            }
        }

        IEnumerator DownloadTexture(WWW req, Action<Texture2D> onLoad, Action<string> onError = null, bool cacheInMemory = true) {
            while(!req.isDone) {
                yield return null;
            }
            while(loadingTextures.Contains(req.url)) {
                yield return null;
            }

            if(cachedTextures.ContainsKey(req.url)) {
                onLoad(cachedTextures[req.url]);
            } else {
                loadingTextures.Add(req.url);

                if(req.error != null && req.error.Length > 0) {
                    if(onError != null) {
                        onError(req.error);
                    }
                    Debug.LogErrorFormat("DYNAMICIMAGEMANAGER could not load image {0}: {1}", req.url, req.error);
                } else {
                    Texture2D image = req.texture;
                    if(image != null) {
                        if(cacheInMemory) {
                            cachedTextures[req.url] = image;
                        }
                        onLoad(image);
                    } else {
                        if(onError != null) {
                            onError("Could not generate an image from the url " + req.url);
                        }
                        Debug.LogErrorFormat("DYNAMICIMAGEMANAGER could not generate an image from the url {0}", req.url);
                    }
                    image = null;
                }

                loadingTextures.Remove(req.url);
            }
        }
    }
}