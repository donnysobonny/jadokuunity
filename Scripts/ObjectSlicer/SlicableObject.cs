﻿using UnityEngine;

namespace Jadoku.ObjectSlicer {
    public class SlicableObject : MonoBehaviour {
        public MeshRenderer meshRenderer;
        public MeshFilter meshFilter;
        public Material crossMaterial;

        public Rigidbody body;
        public MeshCollider meshCollider;

        private Plane slicingPlane = new Plane();

        private void Start() {
            if(this.meshRenderer == null) {
                Debug.LogError("SlicableObject must have a mesh renderer assigned!", this);
            }
            if(this.meshFilter == null) {
                Debug.LogError("SlicableObject must have a mesh filter assigned!", this);
            }
            if(this.crossMaterial == null) {
                Debug.LogError("SlicableObject must have a cross material assigned!", this);
            }
        }

        public SlicableObject[] Slice(Vector3 position, Vector3 direction) {
            this.slicingPlane.Compute(this.transform.InverseTransformPoint(position), this.transform.InverseTransformDirection(direction));
            SlicedHull slice = Slicer.Slice(this, this.slicingPlane, new TextureRegion(0.0f, 0.0f, 1.0f, 1.0f), this.crossMaterial);

            //destroy original
            GameObject.Destroy(this.gameObject);

            if(slice == null) {
                return null;
            }

            SlicableObject upperHull = slice.CreateUpperHull(this, this.crossMaterial);
            SlicableObject lowerHull = slice.CreateLowerHull(this, this.crossMaterial);

            if(upperHull != null && lowerHull != null) {
                return new SlicableObject[] { upperHull, lowerHull };
            }

            // otherwise return only the upper hull
            if(upperHull != null) {
                return new SlicableObject[] { upperHull };
            }

            // otherwise return only the lower hull
            if(lowerHull != null) {
                return new SlicableObject[] { lowerHull };
            }

            // nothing to return, so return nothing!
            return null;
        }

        public virtual void OnSlice() {
            Debug.Log("test1");
        }
    }
}
