﻿using UnityEngine;

namespace Jadoku.ObjectSlicer {

    /**
	 * The final generated data structure from a slice operation. This provides easy access
	 * to utility functions and the final Mesh data for each section of the HULL.
	 */
    public sealed class SlicedHull {
        private static Mesh mesh;

        public SlicedHull(Mesh upperHull, Mesh lowerHull) {
			this.upperHull = upperHull;
			this.lowerHull = lowerHull;
		}

		public SlicableObject CreateUpperHull(SlicableObject original, Material crossSectionMat) {
            SlicableObject newObject = CreateEmptyObject(original, this.upperHull);

			if (newObject != null) {
				newObject.transform.localPosition = original.transform.localPosition;
				newObject.transform.localRotation = original.transform.localRotation;
				newObject.transform.localScale = original.transform.localScale;

                // nothing changed in the hierarchy, the cross section must have been batched
                // with the submeshes, return as is, no need for any changes
                if (original.meshFilter.sharedMesh.subMeshCount == upperHull.subMeshCount) {
                    // the the material information
                    newObject.meshRenderer.sharedMaterials = original.meshRenderer.sharedMaterials;
                    newObject.OnSlice();
                    return newObject;
                }

                // otherwise the cross section was added to the back of the submesh array because
                // it uses a different material. We need to take this into account
                Material[] newShared = new Material[original.meshRenderer.sharedMaterials.Length + 1];

                // copy our material arrays across using native copy (should be faster than loop)
                System.Array.Copy(original.meshRenderer.sharedMaterials, newShared, original.meshRenderer.sharedMaterials.Length);
                newShared[original.meshRenderer.sharedMaterials.Length] = crossSectionMat;

                // the the material information
                newObject.meshRenderer.sharedMaterials = newShared;
                
                newObject.OnSlice();
            }

			return newObject;
		}

		public SlicableObject CreateLowerHull(SlicableObject original, Material crossSectionMat) {
            SlicableObject newObject = CreateEmptyObject(original, this.lowerHull);

			if (newObject != null) {
				newObject.transform.localPosition = original.transform.localPosition;
				newObject.transform.localRotation = original.transform.localRotation;
				newObject.transform.localScale = original.transform.localScale;

                // nothing changed in the hierarchy, the cross section must have been batched
                // with the submeshes, return as is, no need for any changes
                if (original.meshFilter.sharedMesh.subMeshCount == lowerHull.subMeshCount) {
                    // the the material information
                    newObject.meshRenderer.sharedMaterials = original.meshRenderer.sharedMaterials;
					newObject.OnSlice();
                    return newObject;
                }

                // otherwise the cross section was added to the back of the submesh array because
                // it uses a different material. We need to take this into account
                Material[] newShared = new Material[original.meshRenderer.sharedMaterials.Length + 1];

                // copy our material arrays across using native copy (should be faster than loop)
                System.Array.Copy(original.meshRenderer.sharedMaterials, newShared, original.meshRenderer.sharedMaterials.Length);
                newShared[original.meshRenderer.sharedMaterials.Length] = crossSectionMat;

                // the the material information
                newObject.meshRenderer.sharedMaterials = newShared;
                
                newObject.OnSlice();
			}

			return newObject;
		}

        public Mesh upperHull { get; private set; }

        public Mesh lowerHull { get; private set; }

        /**
		 * Helper function which will create a new GameObject to be able to add
		 * a new mesh for rendering and return.
		 */
        private static SlicableObject CreateEmptyObject(SlicableObject orig, Mesh hull) {
			if (hull == null) {
				return null;
			}

            SlicableObject newObject = Object.Instantiate(orig);
            newObject.meshFilter.mesh = hull;
            if(newObject.meshCollider != null) {
                newObject.meshCollider.sharedMesh = hull;
            }
            if(newObject.body != null) {
                newObject.body.ResetCenterOfMass();
            }

            return newObject;
		}
	}
}