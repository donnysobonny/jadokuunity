﻿#if UNITY_2018_0_OR_NEWER

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LitJson;

namespace Jadoku.GameObjectBrush {

    /// <summary>
    /// The main class of this extension/tool that handles the ui and the brush/paint functionality
    /// </summary>
    public class GameObjectBrushEditor : EditorWindow {

        public void SaveSettings() {
            PlayerPrefs.SetString("go_brush_default_properties", JsonMapper.ToJson(this.defaultBrushProperties));
            PlayerPrefs.SetString("go_brush_brushes", JsonMapper.ToJson(this.brushes));
            PlayerPrefs.SetString("go_brush_root", this.objectRoot.name);
            PlayerPrefs.Save();
        }

        public void LoadSettings() {
            if(PlayerPrefs.HasKey("go_brush_default_properties")) {
                this.defaultBrushProperties = JsonMapper.ToObject<BrushProperties>(PlayerPrefs.GetString("go_brush_default_properties"));
            }
            if(PlayerPrefs.HasKey("go_brush_brushes")) {
                this.brushes = JsonMapper.ToObject<List<BrushObject>>(PlayerPrefs.GetString("go_brush_brushes"));
                foreach(BrushObject br in this.brushes) {
                    br.Init();
                }
                this.Repaint();
            }
            if(PlayerPrefs.HasKey("go_brush_root")) {
                this.objectRoot = GameObject.Find(PlayerPrefs.GetString("go_brush_root")).GetComponent<Transform>();
            }
        }

        //version variable
        private static string version = "v3.0";

        //some utility vars used to determine if the editor window is open
        public static GameObjectBrushEditor Instance { get; private set; }
        public static bool IsOpen {
            get { return Instance != null; }
        }

        public Transform objectRoot;

        //custom vars that hold the brushes, the current brush, the copied brush settings/details, the scroll position of the scroll view and all previously spawned objects
        public List<BrushObject> brushes = new List<BrushObject>();
        public BrushObject selectedBrush = null;                                    //The currently selected/viewes brush (has to be public in order to be accessed by the FindProperty method)
        public int selectedBrushIndex = -1;
        private Vector2 scrollViewScrollPosition = new Vector2();

        private bool isPlacingEnabled = true;


        private Color red = new Color((float)186 / 256, (float)48 / 256, (float)48 / 256);
        private Color green = new Color((float)91 / 256,(float)186 / 256, (float)48 / 256);
        private Color darkBlue = new Color((float)14 / 256, (float) 36 / 256, (float) 56 / 256);

        private bool isEditingDefaultProperties = false;
        public BrushProperties defaultBrushProperties = null;

        /// <summary>
        /// Method that creates the window initially
        /// </summary>
        [MenuItem("Jadoku/Tools/GameObject Brush")]
        public static void ShowWindow() {
            //Show existing window instance. If one doesn't exist, make one.
            GameObjectBrushEditor window = GetWindow<GameObjectBrushEditor>("GO Brush " + version);
            DontDestroyOnLoad(window);
            window.LoadSettings();
        }

        void OnEnable() {
            SceneView.onSceneGUIDelegate += SceneGUI;
            Instance = this;
        }
        public void OnGUI() {
            if(this.objectRoot == null) {
                this.objectRoot = GameObject.Find("_GAMEOBJECT_BRUSH_ROOT").GetComponent<Transform>();
                if(this.objectRoot == null) {
                    this.objectRoot = new GameObject("_GAMEOBJECT_BRUSH_ROOT").GetComponent<Transform>();
                }
            }

            SerializedObject so = new SerializedObject(this);
            EditorGUIUtility.wideMode = true;

            this.objectRoot = (Transform)EditorGUILayout.ObjectField(this.objectRoot, typeof(Transform), true);

            EditorGUILayout.Separator();

            EditorGUILayout.LabelField("Your Brushes", EditorStyles.boldLabel);

            //scroll view
            scrollViewScrollPosition = EditorGUILayout.BeginScrollView(scrollViewScrollPosition, false, false);
            EditorGUILayout.BeginHorizontal();
            Color guiColor = GUI.backgroundColor;
            if(this.isEditingDefaultProperties) {
                GUI.backgroundColor = darkBlue;
            }
            if(GUILayout.Button("Default\nBrush\nSettings", GUILayout.Width(100), GUILayout.Height(100))) {
                this.selectedBrush = null;
                this.selectedBrushIndex = -1;
                this.isEditingDefaultProperties = true;
            }
            GUI.backgroundColor = guiColor;

            for(int i = 0; i < this.brushes.Count; i++) {
                guiColor = GUI.backgroundColor;
                if(this.selectedBrushIndex == i) {
                    GUI.backgroundColor = darkBlue;
                }

                //Create the brush entry in the scroll view and check if the user clicked on the created button (change the currently selected/edited brush accordingly and add it to the current brushes if possible)
                GUIContent btnContent = new GUIContent(AssetPreview.GetAssetPreview(this.brushes[i].GetBrushObject()), "Select the " + this.brushes[i].GetBrushObject().name + " brush");
                if(GUILayout.Button(btnContent, GUILayout.Width(100), GUILayout.Height(100))) {
                    this.selectedBrush = this.brushes[i];
                    this.selectedBrushIndex = i;
                    this.isEditingDefaultProperties = false;
                }
                GUI.backgroundColor = guiColor;
            }

            //add button
            if (GUILayout.Button("+", GUILayout.Width(100), GUILayout.Height(100))) {
                AddObjectPopup.Init(brushes, this);
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndScrollView();

            //gui below the scroll view
            Color guiColorBGC = GUI.backgroundColor;
            
            if(this.isEditingDefaultProperties) {
                EditorGUILayout.BeginVertical();

                EditorGUILayout.LabelField("Default Brush Settings", EditorStyles.boldLabel);

                if(this.defaultBrushProperties == null) {
                    this.defaultBrushProperties = new BrushProperties();
                }

                this.defaultBrushProperties.snapToGrid = EditorGUILayout.Toggle(new GUIContent("Snap To Grid", "Placed objects will snap to the specified grid cell sizes/offsets"), this.defaultBrushProperties.snapToGrid);
                if(selectedBrush.snapToGrid) {
                    this.defaultBrushProperties.gridCellSize = EditorGUILayout.Vector3Field(new GUIContent("Grid Cell Size", "The grid cell size"), this.defaultBrushProperties.gridCellSize);
                    this.defaultBrushProperties.gridCellOffset = EditorGUILayout.Vector3Field(new GUIContent("Grid Cell Offset", "The grid cell offset"), this.defaultBrushProperties.gridCellOffset);
                }

                this.defaultBrushProperties.density = EditorGUILayout.Slider(new GUIContent("Density", "Changes the density of the brush, i.e. how many gameobjects are spawned inside the radius of the brush."), this.defaultBrushProperties.density, 0.1f, 5f);
                this.defaultBrushProperties.brushSize = EditorGUILayout.Slider(new GUIContent("Brush Size", "The radius of the brush."), this.defaultBrushProperties.brushSize, 0f, 25f);
                this.defaultBrushProperties.offsetFromPivot = EditorGUILayout.Vector3Field(new GUIContent("Offset from Pivot", "Changes the offset of the spawned gameobject from the calculated position. This allows you to correct the position of the spawned objects, if you find they are floating for example due to a not that correct pivot on the gameobject/prefab."), this.defaultBrushProperties.offsetFromPivot);
                this.defaultBrushProperties.rotOffsetFromPivot = EditorGUILayout.Vector3Field(new GUIContent("Rotational Offset", "Changes the rotational offset that is applied to the prefab/gameobject when spawning it. This allows you to current the rotation of the spawned objects."), this.defaultBrushProperties.rotOffsetFromPivot);


                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent("Min and Max Scale", "The min and max range of the spawned gameobject. If they are not the same value a random value in between the min and max is going to be picked."));
                EditorGUILayout.MinMaxSlider(ref this.defaultBrushProperties.minScale, ref this.defaultBrushProperties.maxScale, 0.001f, 50);
                this.defaultBrushProperties.minScale = EditorGUILayout.FloatField(this.defaultBrushProperties.minScale);
                this.defaultBrushProperties.maxScale = EditorGUILayout.FloatField(this.defaultBrushProperties.maxScale);
                EditorGUILayout.EndHorizontal();

                this.defaultBrushProperties.alignToSurface = EditorGUILayout.Toggle(new GUIContent("Align to Surface", "This option allows you to align the instantiated gameobjects to the surface you are painting on."), this.defaultBrushProperties.alignToSurface);

                EditorGUILayout.BeginHorizontal();
                this.defaultBrushProperties.randomizeXRotation = EditorGUILayout.Toggle(new GUIContent("Randomize X Rotation", "Should the rotation be randomized on the x axis?"), this.defaultBrushProperties.randomizeXRotation);
                this.defaultBrushProperties.randomizeYRotation = EditorGUILayout.Toggle(new GUIContent("Randomize Y Rotation", "Should the rotation be randomized on the y axis?"), this.defaultBrushProperties.randomizeYRotation);
                this.defaultBrushProperties.randomizeZRotation = EditorGUILayout.Toggle(new GUIContent("Randomize Z Rotation", "Should the rotation be randomized on the z axis?"), this.defaultBrushProperties.randomizeZRotation);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();
                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent("Min and Max Slope", "The range of slope that is required for an object to be placed. If the slope is not in that range, no object is going to be placed."));
                EditorGUILayout.MinMaxSlider(ref this.defaultBrushProperties.minSlope, ref this.defaultBrushProperties.maxSlope, 0, 360);
                this.defaultBrushProperties.minSlope = EditorGUILayout.FloatField(this.defaultBrushProperties.minSlope);
                this.defaultBrushProperties.maxSlope = EditorGUILayout.FloatField(this.defaultBrushProperties.maxSlope);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.PropertyField(so.FindProperty("defaultBrushProperties").FindPropertyRelative("layerFilter"), true);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PropertyField(so.FindProperty("defaultBrushProperties").FindPropertyRelative("isTagFilteringEnabled"), true);
                if(this.defaultBrushProperties.isTagFilteringEnabled) {
                    this.defaultBrushProperties.tagFilter = EditorGUILayout.TagField(new GUIContent("Tag Filter", "Limits the painting to objects that have a specific tag on them."), this.defaultBrushProperties.tagFilter);
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                so.ApplyModifiedProperties();

                EditorGUILayout.EndVertical();
            } else if(selectedBrush != null && selectedBrush.GetBrushObject() != null) {
                EditorGUILayout.BeginHorizontal();

                GUI.backgroundColor = green;
                if(GUILayout.Button(new GUIContent("Add Brush", "Add a new brush to the selection."))) {
                    AddObjectPopup.Init(brushes, this);
                }
                GUI.backgroundColor = red;
                if(GUILayout.Button(new GUIContent("Remove Current Brush(es)", "Removes the currently selected brush."))) {
                    this.brushes.Remove(this.selectedBrush);
                    this.selectedBrush = null;
                    this.selectedBrushIndex = -1;

                    return;
                }
                if(GUILayout.Button(new GUIContent("Clear Brushes", "Removes all brushes."))) {
                    brushes.Clear();
                    this.selectedBrush = null;
                    this.selectedBrushIndex = -1;

                    return;
                }
                EditorGUILayout.EndHorizontal();
                GUI.backgroundColor = guiColorBGC;

                EditorGUILayout.Space();
                EditorGUILayout.BeginHorizontal();
                isPlacingEnabled = EditorGUILayout.Toggle(new GUIContent("Painting enabled", "Should painting of gameobjects via left click be enabled?"), isPlacingEnabled);
                EditorGUILayout.EndHorizontal();
                if(this.isPlacingEnabled) {
                    EditorGUILayout.HelpBox(new GUIContent("Click/Hold to paint the object\n\nHold shift to erase any objects within the currently managed root\n\nHold Ctrl to erase only objects of the selected type"), true);
                }
                EditorGUILayout.Space();

                selectedBrush.snapToGrid = EditorGUILayout.Toggle(new GUIContent("Snap To Grid", "Placed objects will snap to the specified grid cell sizes/offsets"), selectedBrush.snapToGrid);
                if(selectedBrush.snapToGrid) {
                    selectedBrush.gridCellSize = EditorGUILayout.Vector3Field(new GUIContent("Grid Cell Size", "The grid cell size"), selectedBrush.gridCellSize);
                    selectedBrush.gridCellOffset = EditorGUILayout.Vector3Field(new GUIContent("Grid Cell Offset", "The grid cell offset"), selectedBrush.gridCellOffset);
                }

                selectedBrush.density = EditorGUILayout.Slider(new GUIContent("Density", "Changes the density of the brush, i.e. how many gameobjects are spawned inside the radius of the brush."), selectedBrush.density, 0.1f, 5f);
                selectedBrush.brushSize = EditorGUILayout.Slider(new GUIContent("Brush Size", "The radius of the brush."), selectedBrush.brushSize, 0f, 25f);
                selectedBrush.offsetFromPivot = EditorGUILayout.Vector3Field(new GUIContent("Offset from Pivot", "Changes the offset of the spawned gameobject from the calculated position. This allows you to correct the position of the spawned objects, if you find they are floating for example due to a not that correct pivot on the gameobject/prefab."), selectedBrush.offsetFromPivot);
                selectedBrush.rotOffsetFromPivot = EditorGUILayout.Vector3Field(new GUIContent("Rotational Offset", "Changes the rotational offset that is applied to the prefab/gameobject when spawning it. This allows you to current the rotation of the spawned objects."), selectedBrush.rotOffsetFromPivot);


                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent("Min and Max Scale", "The min and max range of the spawned gameobject. If they are not the same value a random value in between the min and max is going to be picked."));
                EditorGUILayout.MinMaxSlider(ref selectedBrush.minScale, ref selectedBrush.maxScale, 0.001f, 50);
                selectedBrush.minScale = EditorGUILayout.FloatField(selectedBrush.minScale);
                selectedBrush.maxScale = EditorGUILayout.FloatField(selectedBrush.maxScale);
                EditorGUILayout.EndHorizontal();

                selectedBrush.alignToSurface = EditorGUILayout.Toggle(new GUIContent("Align to Surface", "This option allows you to align the instantiated gameobjects to the surface you are painting on."), selectedBrush.alignToSurface);

                EditorGUILayout.BeginHorizontal();
                selectedBrush.randomizeXRotation = EditorGUILayout.Toggle(new GUIContent("Randomize X Rotation", "Should the rotation be randomized on the x axis?"), selectedBrush.randomizeXRotation);
                selectedBrush.randomizeYRotation = EditorGUILayout.Toggle(new GUIContent("Randomize Y Rotation", "Should the rotation be randomized on the y axis?"), selectedBrush.randomizeYRotation);
                selectedBrush.randomizeZRotation = EditorGUILayout.Toggle(new GUIContent("Randomize Z Rotation", "Should the rotation be randomized on the z axis?"), selectedBrush.randomizeZRotation);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();
                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent("Min and Max Slope", "The range of slope that is required for an object to be placed. If the slope is not in that range, no object is going to be placed."));
                EditorGUILayout.MinMaxSlider(ref selectedBrush.minSlope, ref selectedBrush.maxSlope, 0, 360);
                selectedBrush.minSlope = EditorGUILayout.FloatField(selectedBrush.minSlope);
                selectedBrush.maxSlope = EditorGUILayout.FloatField(selectedBrush.maxSlope);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.PropertyField(so.FindProperty("selectedBrush").FindPropertyRelative("layerFilter"), true);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PropertyField(so.FindProperty("selectedBrush").FindPropertyRelative("isTagFilteringEnabled"), true);
                if (selectedBrush.isTagFilteringEnabled) {
                    selectedBrush.tagFilter = EditorGUILayout.TagField(new GUIContent("Tag Filter", "Limits the painting to objects that have a specific tag on them."), selectedBrush.tagFilter);
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                so.ApplyModifiedProperties();
            } else {
                EditorGUILayout.HelpBox(new GUIContent("Select an existing brush above, or add a new one below to get started"), true);

                EditorGUILayout.BeginHorizontal();

                GUI.backgroundColor = green;
                if(GUILayout.Button(new GUIContent("Add Brush", "Add a new brush to the selection."))) {
                    AddObjectPopup.Init(brushes, this);
                }
                GUI.backgroundColor = red;
                if(GUILayout.Button(new GUIContent("Remove Current Brush(es)", "Removes the currently selected brush."))) {
                    this.brushes.Remove(this.selectedBrush);
                    this.selectedBrush = null;
                    this.selectedBrushIndex = -1;
                }
                if(GUILayout.Button(new GUIContent("Clear Brushes", "Removes all brushes."))) {
                    brushes.Clear();
                    this.selectedBrush = null;
                    this.selectedBrushIndex = -1;
                }
                EditorGUILayout.EndHorizontal();
                GUI.backgroundColor = guiColorBGC;

                EditorGUILayout.Space();
            }

            this.SaveSettings();
        }
        public void OnDestroy() {
            SceneView.onSceneGUIDelegate -= SceneGUI;
        }
        /// <summary>
        /// Delegate that handles Scene GUI events
        /// </summary>
        /// <param name="sceneView"></param>
        void SceneGUI(SceneView sceneView) {

            //don't do anything if the gameobject brush window is not open
            if(!IsOpen) {
                return;
            }

            if(this.isPlacingEnabled && this.selectedBrush != null && Tools.current != Tool.View) {
                //Draw Brush in the scene view
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                RaycastHit hit;
                if(Physics.Raycast(ray, out hit, Mathf.Infinity, this.selectedBrush.layerFilter)) {
                    Color color = Color.cyan;
                    color.a = 0.25f;
                    Handles.color = color;
                    Handles.DrawSolidArc(hit.point, hit.normal, Vector3.Cross(hit.normal, ray.direction), 360, GetMaximumBrushSizeFromCurrentBrushes());
                    Handles.color = Color.white;
                    Handles.DrawLine(hit.point, hit.point + hit.normal * 5);
                }

                if(Event.current.rawType == EventType.MouseDown || Event.current.rawType == EventType.MouseDrag) {
                    if(Event.current.button == 0 && !Event.current.shift && !Event.current.control) {
                        this.PlaceObjects();
                        Event.current.Use();
                        GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    } else if(Event.current.button == 0 && Event.current.shift && !Event.current.control) {
                        this.RemoveObjects(false);
                        Event.current.Use();
                        GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    } else if(Event.current.button == 0 && !Event.current.shift && Event.current.control) {
                        this.RemoveObjects(true);
                        Event.current.Use();
                        GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    }
                }

                SceneView.RepaintAll();
            }
        }

        #region GO Brush functionality methods

        private float Snap(float value, float multipleOf) {
            return Mathf.Round(value / multipleOf) * multipleOf;
        }

        Vector2 oldMousePos = Vector2.zero;
        private void PlaceObjects() {
            //only paint if painting is ebanled
            if (!isPlacingEnabled) {
                return;
            }

            if(Vector2.Distance(Event.current.mousePosition, this.oldMousePos) < this.selectedBrush.brushSize / this.selectedBrush.density) {
                return;
            }
            this.oldMousePos = Event.current.mousePosition;

            if(this.selectedBrush != null) {
                //create gameobjects of the given type if possible
                if(this.selectedBrush.GetBrushObject() != null && IsOpen) {

                    //raycast from the scene camera to find the position of the brush and create objects there
                    Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                    ray.origin += new Vector3(Random.Range(-this.selectedBrush.brushSize, this.selectedBrush.brushSize), Random.Range(-this.selectedBrush.brushSize, this.selectedBrush.brushSize), Random.Range(-this.selectedBrush.brushSize, this.selectedBrush.brushSize));
                    Vector3 startPoint = ray.origin;
                    RaycastHit hit;

                    if(Physics.Raycast(ray, out hit)) {
                        //calculate the angle and abort if it is not in the specified range/filter
                        float angle = Vector3.Angle(Vector3.up, hit.normal);
                        if(angle < this.selectedBrush.minSlope || angle > this.selectedBrush.maxSlope) {
                            return;
                        }

                        //check if the layer of the hit object is in our layermask filter
                        if(this.selectedBrush.layerFilter != (this.selectedBrush.layerFilter | (1 << hit.transform.gameObject.layer))) {
                            return;
                        }

                        //check if tag filtering is active, if so check the tags
                        if(this.selectedBrush.isTagFilteringEnabled && hit.transform.tag != this.selectedBrush.tagFilter) {
                            return;
                        }

                        //randomize position
                        Vector3 position = hit.point + this.selectedBrush.offsetFromPivot;
                        if(this.selectedBrush.snapToGrid) {
                            position.x = this.Snap(position.x, this.selectedBrush.gridCellSize.x) + this.selectedBrush.gridCellOffset.x;
                            position.y = this.Snap(position.y, this.selectedBrush.gridCellSize.y) + this.selectedBrush.gridCellOffset.y;
                            position.z = this.Snap(position.z, this.selectedBrush.gridCellSize.z) + this.selectedBrush.gridCellOffset.z;
                        }

                        //instantiate object
                        GameObject obj = Instantiate(this.selectedBrush.GetBrushObject(), position, Quaternion.identity);

                        //register created objects to the undo stack
                        Undo.RegisterCreatedObjectUndo(obj, "Created " + obj.name + " with brush");

                        //check if we should align the object to the surface we are "painting" on
                        if(this.selectedBrush.alignToSurface) {
                            obj.transform.up = hit.normal;
                        }

                        //Randomize rotation
                        Vector3 rot = this.selectedBrush.rotOffsetFromPivot;
                        if(this.selectedBrush.randomizeXRotation)
                            rot.x = Random.Range(0, 360);
                        if(this.selectedBrush.randomizeYRotation)
                            rot.y = Random.Range(0, 360);
                        if(this.selectedBrush.randomizeZRotation)
                            rot.z = Random.Range(0, 360);

                        //apply rotation
                        obj.transform.Rotate(rot, Space.Self);

                        //randomize scale
                        float scale = Random.Range(this.selectedBrush.minScale, this.selectedBrush.maxScale);
                        obj.transform.localScale = new Vector3(scale, scale, scale);

                        obj.transform.SetParent(this.objectRoot);
                    }
                }
            }
        }

        private void RemoveObjects(bool targeted = false) {

            //return if erasing is disabled
            if (!this.isPlacingEnabled) {
                return;
            }            

            if(this.selectedBrush != null) {
                //raycast to fin brush position
                RaycastHit hit;
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                if(Physics.Raycast(ray, out hit)) {
                    Transform[] ts = this.objectRoot.GetComponentsInChildren<Transform>();
                    foreach(Transform t in ts) {
                        if(t != this.objectRoot && t.parent == this.objectRoot) {
                            if(targeted && this.selectedBrush != null) {
                                if(t.gameObject.name == this.selectedBrush.GetBrushObject().name + "(Clone)" && Vector3.Distance(t.position, hit.point) < this.selectedBrush.brushSize) {
                                    DestroyImmediate(t.gameObject);
                                }
                            } else {
                                if(Vector3.Distance(t.position, hit.point) < this.selectedBrush.brushSize) {
                                    DestroyImmediate(t.gameObject);
                                }
                            }
                        }                        
                    }
                }
            }
        }

        #endregion

        #region Misc


        /// <summary>
        /// Iterates over the list of current brushes and adds the name of each brush to a string.
        /// </summary>
        /// <returns></returns>
        private string GetCurrentBrushesString() {
            string brushes = "";
            if(this.selectedBrush != null) {
                return this.selectedBrush.GetBrushObject().name;
            }
            return brushes;
        }
        /// <summary>
        /// Get the greatest brush size value from the current brushes list
        /// </summary>
        /// <returns></returns>
        private float GetMaximumBrushSizeFromCurrentBrushes() {
            float maxBrushSize = 0f;
            if(this.selectedBrush != null) {
                return this.selectedBrush.brushSize;
            }
            return maxBrushSize;
        }
        
        
        #endregion
    }

    [System.Serializable]
    public class BrushProperties {
        public bool snapToGrid = false;
        public Vector3 gridCellSize = Vector3.zero;
        public Vector3 gridCellOffset = Vector3.zero;
        public bool alignToSurface = false;
        [Tooltip("Should the rotation be randomized on the x axis?")] public bool randomizeXRotation = false;
        [Tooltip("Should the rotation be randomized on the y axis?")] public bool randomizeYRotation = true;
        [Tooltip("Should the rotation be randomized on the z axis?")] public bool randomizeZRotation = false;
        [Range(0.1f, 10f)] public float density = 1f;
        [Range(0f, 100f)] public float brushSize = 5f;
        [Range(0f, 10f)] public float minScale = 0.5f;
        [Range(0f, 10f)] public float maxScale = 1.5f;
        [Tooltip("The offset applied to the pivot of the brushObject. This is usefull if you find that the placed GameObjects are floating/sticking in the ground too much.")] public Vector3 offsetFromPivot = Vector3.zero;
        [Tooltip("The offset applied to the rotation of the brushObject.")] public Vector3 rotOffsetFromPivot = Vector3.zero;

        /* filters */
        [Range(0, 360)] public float minSlope = 0f;
        [Range(0, 360)] public float maxSlope = 360f;
        public LayerMask layerFilter = ~0;

        public bool isTagFilteringEnabled = false;
        public string tagFilter = "";
    }
    
    [System.Serializable]
    public class BrushObject : BrushProperties {
        private GameObject brushObject;
        public GameObject GetBrushObject() {
            return this.brushObject;
        }
        public string brushObjectPath = "";

        public BrushObject(GameObject obj, BrushProperties defaultProperties) {
            this.brushObject = obj;
            this.brushObjectPath = AssetDatabase.GetAssetPath(obj);

            this.snapToGrid = defaultProperties.snapToGrid;
            this.gridCellSize = defaultProperties.gridCellSize;
            this.gridCellOffset = defaultProperties.gridCellOffset;

            this.alignToSurface = defaultProperties.alignToSurface;
            this.randomizeXRotation = defaultProperties.randomizeXRotation;
            this.randomizeYRotation = defaultProperties.randomizeYRotation;
            this.randomizeZRotation = defaultProperties.randomizeZRotation;
            this.density = defaultProperties.density;
            this.brushSize = defaultProperties.brushSize;
            this.minScale = defaultProperties.minScale;
            this.maxScale = defaultProperties.maxScale;

            this.offsetFromPivot = defaultProperties.offsetFromPivot;
            this.rotOffsetFromPivot = defaultProperties.rotOffsetFromPivot;

            this.minSlope = defaultProperties.minSlope;
            this.maxSlope = defaultProperties.maxSlope;
            this.layerFilter = defaultProperties.layerFilter;

            this.isTagFilteringEnabled = defaultProperties.isTagFilteringEnabled;
            this.tagFilter = defaultProperties.tagFilter;
        }

        public BrushObject() { }
        public void Init() {
            this.brushObject = AssetDatabase.LoadAssetAtPath<GameObject>(this.brushObjectPath);
        }
    }
}

#endif