﻿using UnityEngine;

namespace Jadoku.Gfx {
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteRendererBlink : MonoBehaviour {
        public SpriteRenderer spriteRenderer;
        public float blinkInterval;
        public bool blinkOnStart = false;

        private void Start() {
            if(this.blinkOnStart) {
                this.StartBlink();
            }
        }

        private void OnEnable() {
            if(this.blinkOnStart) {
                this.StartBlink();
            }
        }

        bool blinking = false;
        float nextBlink = 0f;
        public void StartBlink() {
            this.blinking = true;
            this.spriteRenderer.enabled = true;
            this.nextBlink = Time.time + this.blinkInterval;
        }
        public void StopBlink() {
            this.spriteRenderer.enabled = true;
            this.blinking = false;
        }

        private void Update() {
            if(this.blinking && Time.time > this.nextBlink) {
                this.nextBlink = Time.time + this.blinkInterval;
                this.spriteRenderer.enabled = !this.spriteRenderer.enabled;
            }
        }
    }
}
