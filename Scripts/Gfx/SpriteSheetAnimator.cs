﻿using Jadoku.Gfx.Helpers;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Jadoku.Gfx {
    public class SpriteSheetAnimator : MonoBehaviour {
        public SpriteRenderer spriteRenderer;
        public SpriteSheetAnimation[] spriteSheetAnimations = new SpriteSheetAnimation[] { };
        public bool playOnStart = false;
        public bool cullOffScreen = false;

        void Start() {
            if (playOnStart && this.spriteSheetAnimations.Length > 0) {
                this.Loop(0);
            }
        }

        private bool animating = false;
        private bool loop = false;
        private SpriteSheetAnimation targetAnimation = new SpriteSheetAnimation();
        private int frame = -1;
        private float frameSpeed = 0.1f;
        private float nextFrame = 0f;
        private float deltaTime = 0f;
        private int targetAnimationIndex;

        void Update() {
            if (this.cullOffScreen && !this.spriteRenderer.isVisible) {
                return;
            }

            if (this.animating) {
                this.deltaTime += Time.deltaTime;

                while (this.deltaTime >= this.frameSpeed) {
                    this.deltaTime -= this.frameSpeed;
                    this.NextFrame();
                }
            }
        }

        /// <summary>
        /// Play the selected animation once
        /// </summary>
        /// <param name="animationIndex"></param>
        public void Play(int animationIndex) {
            this.SetAnimation(animationIndex);
            this.Start(1f, false);
        }

        /// <summary>
        /// Play the selected animation once, at the specified speed
        /// </summary>
        /// <param name="animationIndex"></param>
        /// <param name="speed"></param>
        public void Play(int animationIndex, float speed) {
            this.SetAnimation(animationIndex);
            this.Start(speed, false);
        }

        public void Start(float speed = 1f, bool loop = false) {
            this.SetSpeed(speed);
            this.SetFrame(-1);
            this.loop = loop;
            this.nextFrame = 0f;
            this.animating = true;
        }

        /// <summary>
        /// Stop all animating
        /// </summary>
        public void Stop() {
            this.animating = false;
        }

        /// <summary>
        /// Resume a stopped animation
        /// </summary>
        public void Resume() {
            this.animating = true;
        }

        /// <summary>
        /// Loop the specified animation
        /// </summary>
        /// <param name="animationIndex"></param>
        public void Loop(int animationIndex) {
            this.SetAnimation(animationIndex);
            this.Start(1f, true);
        }

        /// <summary>
        /// Loop the specified animation, at the specified speed
        /// </summary>
        /// <param name="animationIndex"></param>
        /// <param name="speed"></param>
        public void Loop(int animationIndex, float speed) {
            this.SetAnimation(animationIndex);
            this.Start(speed, true);
        }

        /// <summary>
        /// Set the speed of the current animation (gets calculated to offset the frame time of the animation)
        /// </summary>
        /// <param name="speed"></param>
        public void SetSpeed(float speed) {
            float newSpeed = Mathf.Clamp((1f / speed) * this.targetAnimation.frameTime, 0f, this.targetAnimation.frameTime * 1000000f);
            //if the speed has changed, reset the frame time
            if (newSpeed != this.frameSpeed) {
                this.frameSpeed = newSpeed;
                float targetNextFrame = Time.realtimeSinceStartup + this.frameSpeed;
                if (targetNextFrame < this.nextFrame) {
                    this.nextFrame = targetNextFrame;
                }
            }
        }

        /// <summary>
        /// Set the actual frame speed
        /// </summary>
        /// <param name="frameSpeed"></param>
        public void SetFrameSpeed(float frameSpeed) {
            frameSpeed = Mathf.Clamp(frameSpeed, 0f, this.targetAnimation.frameTime * 1000000f);
            //if the speed has changed
            if (frameSpeed != this.frameSpeed) {
                this.frameSpeed = frameSpeed;
                float targetNextFrame = Time.realtimeSinceStartup + this.frameSpeed;
                if (targetNextFrame < this.nextFrame) {
                    this.nextFrame = targetNextFrame;
                }
            }
        }

        /// <summary>
        /// Get an animation by it's array index
        /// </summary>
        /// <param name="animationIndex"></param>
        /// <returns></returns>
        public SpriteSheetAnimation GetAnimation(int animationIndex) {
            if (this.spriteSheetAnimations[animationIndex] != null) {
                return this.spriteSheetAnimations[animationIndex];
            }
            return null;
        }

        /// <summary>
        /// Get the current animation
        /// </summary>
        /// <returns></returns>
        public SpriteSheetAnimation GetCurrentAnimation() {
            return this.targetAnimation;
        }

        /// <summary>
        /// Get the current animation index
        /// </summary>
        /// <returns></returns>
        public int GetCurrentAnimationIndex() {
            return this.targetAnimationIndex;
        }

        /// <summary>
        /// Get the current frame
        /// </summary>
        /// <returns></returns>
        public int GetCurrentFrame() {
            return this.frame;
        }

        /// <summary>
        /// Get the current frame speed
        /// </summary>
        /// <returns></returns>
        public float GetCurrentFrameSpeed() {
            return this.frameSpeed;
        }

        /// <summary>
        /// Manually set the animation by it's array index. Useful for manual control
        /// </summary>
        /// <param name="animationIndex"></param>
        public void SetAnimation(int animationIndex) {
            this.targetAnimationIndex = animationIndex;
            this.targetAnimation = this.GetAnimation(animationIndex);
            if (this.targetAnimation == null) {
                throw new System.Exception("Could not set the animation, as there is no animation at the index: " + animationIndex);
            }
        }

        /// <summary>
        /// Set the frame of the current animation. Useful for manual control
        /// </summary>
        /// <param name="frame"></param>
        public void SetFrame(int frame) {
            this.frame = frame;
            if (this.frame <= 0) {
                this.frame = 0;
            } else if (this.frame >= this.targetAnimation.sprites.Length - 1) {
                this.frame = this.targetAnimation.sprites.Length - 1;
            }
            this.Render();
        }

        /// <summary>
        /// Check whether there is an animation running
        /// </summary>
        /// <returns></returns>
        public bool IsAnimating() {
            return this.animating;
        }

        private void NextFrame() {
            if (this.frameSpeed > 0f) {
                //increment a frame at a time
                this.frame++;

                if (this.frame > this.targetAnimation.sprites.Length - 1) {
                    if (this.loop) {
                        this.frame = 0;
                    } else {
                        this.Stop();
                    }
                }
                //we may have stopped if we reached the last frame, only render if we are still animating...
                if (this.animating) {
                    this.Render();
                }
            }
        }

        private void Render() {
            this.spriteRenderer.sprite = this.targetAnimation.sprites[this.frame];
        }
    }
}