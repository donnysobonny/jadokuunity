﻿using System;
using UnityEngine;

namespace Jadoku.Gfx.Helpers {
    [Serializable]
    public class SpriteSheetAnimation {
        public string name;
        public Sprite[] sprites;
        public float frameTime = 0.1f;
    }
}